PROG = tinybox
OBJS = main.o basename.o cat.o cp.o diff.o \
       dirname.o gzip.o id.o kilo.o ls.o mv.o \
       pwd.o realpath.o rm.o rmdir.o sed.o \
       sh.o sum.o tar.o test.o

$(PROG): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $(OBJS) -lz -o $@

clean:
	$(RM) $(PROG) $(OBJS)
