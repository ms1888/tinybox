/*
 * Copyright © 2021 Plan 9 Foundation
 * Portions Copyright © 2001-2008 Russ Cox
 * Portions Copyright © 2008-2009 Google Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * tar - `tape archiver', actually usable on any medium.
 *	POSIX "ustar" compliant when extracting, and by default when creating.
 *	this tar attempts to read and write multiple Tblock-byte blocks
 *	at once to and from the filesystem, and does not copy blocks
 *	around internally.
 */

#include "extern.h"

#include <assert.h>
#include <ctype.h>
#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <grp.h>
#include <limits.h>
#include <pwd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>

#define HOWMANY(a, size)	(((a) + (size) - 1) / (size))
#define BYTES2TBLKS(bytes)	HOWMANY(bytes, Tblock)

/* read big-endian binary integers; args must be (uchar *) */
#define	G2BEBYTE(x)	(((x)[0]<<8)  |  (x)[1])
#define	G3BEBYTE(x)	(((x)[0]<<16) | ((x)[1]<<8)  |  (x)[2])
#define	G4BEBYTE(x)	(((x)[0]<<24) | ((x)[1]<<16) | ((x)[2]<<8) | (x)[3])
#define	G8BEBYTE(x)	(((long long)G4BEBYTE(x)<<32) | (uint32_t)G4BEBYTE((x)+4))

typedef long long Off;
typedef char *(*Refill)(int ar, char *bufs, int justhdr);

enum { Rd, Wr };			/* pipe fd-array indices */
enum { Output, Input };
enum { None, Toc, Xtract, Replace };
enum { Alldata, Justnxthdr };
enum {
	Tblock = 512,
	Namsiz = 100,
	Maxpfx = 155,		/* from POSIX */
	Maxname = Namsiz + 1 + Maxpfx,
	Binsize = 0x80,		/* flag in size[0], from gnu: positive binary size */
	Binnegsz = 0xff,	/* flag in size[0]: negative binary size */

	Nblock = 40,		/* maximum blocksize */
	Dblock = 20,		/* default blocksize */
	Debug = 0,
};

/* POSIX link flags */
enum {
	LF_PLAIN1 =	'\0',
	LF_PLAIN2 =	'0',
	LF_LINK =	'1',
	LF_SYMLINK1 =	'2',
	LF_SYMLINK2 =	's',		/* 4BSD used this */
	LF_CHR =	'3',
	LF_BLK =	'4',
	LF_DIR =	'5',
	LF_FIFO =	'6',
	LF_CONTIG =	'7',
	/* 'A' - 'Z' are reserved for custom implementations */
};

#define islink(lf)	(isreallink(lf) || issymlink(lf))
#define isreallink(lf)	((lf) == LF_LINK)
#define issymlink(lf)	((lf) == LF_SYMLINK1 || (lf) == LF_SYMLINK2)

typedef union {
	unsigned char	data[Tblock];
	struct {
		char	name[Namsiz];
		char	mode[8];
		char	uid[8];
		char	gid[8];
		char	size[12];
		char	mtime[12];
		char	chksum[8];
		char	linkflag;
		char	linkname[Namsiz];

		/* rest are defined by POSIX's ustar format; see p1003.2b */
		char	magic[6];	/* "ustar" */
		char	version[2];
		char	uname[32];
		char	gname[32];
		char	devmajor[8];
		char	devminor[8];
		char	prefix[Maxpfx]; /* if non-null, path= prefix "/" name */
	};
} Hdr;

typedef struct {
	char	*comp;
	char	*decomp;
	char	*sfx[4];
} Compress;

static Compress comps[] = {
	{"gzip",		"zcat",	{ ".tar.gz", ".tgz" }},	/* default */
	{"compress",	"uncompress",	{ ".tar.Z",  ".tz" }},
	{"bzip2",	"bunzip2",	{ ".tar.bz", ".tbz",
					  ".tar.bz2",".tbz2" }},
	{"xz",			"unxz",	{ ".tar.xz",  ".txz" }},
	{"lzma",	"unlzma",	{ ".tar.lzma",  ".tlz" }},
	{"zstd",	"unzstd",	{ ".tar.zst",  ".tzst" }},
};

typedef struct {
	int	kid;
	int	fd;	/* original fd */
	int	rfd;	/* replacement fd */
	int	input;
	int	open;
} Pushstate;

#define OTHER(rdwr) ((rdwr) == Rd? Wr: Rd)

static int verb;
static int posix = 1;
static int docreate;
static int relative = 1;
static int settime;
static int verbose;
static int docompress;
static int keepexisting;
static int ignerrs;		/* flag: ignore i/o errors if possible */
static Off blkoff;		/* offset of the current archive block (not Tblock) */
static Off nexthdr;

static int nblock = Dblock;
static int resync;
static char *usefile, *arname = "archive";
static char origdir[Maxname*2];
static Hdr *tpblk, *endblk;
static Hdr *curblk;

static void
usage(void)
{
	extern char *__progname;
	fprintf(stderr, "usage: %s {crtx}[PRTfikmpsvz] [archive] [file1 file2...]\n",
		__progname);
	exit(2);
}

/* I/O, with error retry or exit */

static int
cope(char *name, int fd, void *buf, long len, Off off)
{
	warn("%serror reading %s",
		(ignerrs? "ignoring ": ""), name);
	if (!ignerrs)
		exit(2);

	/* pretend we read len bytes of zeroes */
	memset(buf, 0, len);
	if (off >= 0)			/* seekable? */
		lseek(fd, off + len, SEEK_SET);
	return len;
}

static int
eread(char *name, int fd, void *buf, long len)
{
	int rd;
	Off off;

	off = lseek(fd, 0, SEEK_CUR);		/* for coping with errors */
	rd = read(fd, buf, len);
	if (rd < 0)
		rd = cope(name, fd, buf, len, off);
	return rd;
}

static int
ereadn(char *name, int fd, void *buf, long len)
{
	int m, t = 0;
	Off off;

	off = lseek(fd, 0, SEEK_CUR);
	while (t < len) {
		m = read(fd, (char*)buf+t, len-t);
		if (m < 0 && t == 0)
			return cope(name, fd, buf, len, off);
		else if (m <= 0)
			return t;
		else
			t += m;
	}
	return len;
}


/* compression */

static Compress *
compmethod(char *name)
{
	size_t i, nmlen, sfxlen;
	Compress *cp;

	nmlen = strlen(name);
	for (cp = comps; cp < comps + sizeof(comps)/sizeof(comps[0]); cp++)
		for (i = 0; i < sizeof(cp->sfx)/sizeof(cp->sfx[0]) && cp->sfx[i]; i++) {
			sfxlen = strlen(cp->sfx[i]);
			if (nmlen > sfxlen &&
			    strcmp(cp->sfx[i], name+nmlen-sfxlen) == 0)
				return cp;
		}
	return docompress? comps: NULL;
}

/*
 * push a filter, cmd, onto fd.  if input, it's an input descriptor.
 * returns a descriptor to replace fd, or -1 on error.
 */
static int
push(int fd, char *cmd, int input, Pushstate *ps)
{
	extern char *__progname;
	int nfd, pifds[2];

	ps->open = 0;
	ps->fd = fd;
	ps->input = input;
	if (fd < 0 || pipe(pifds) < 0)
		return -1;
	ps->kid = fork();
	switch (ps->kid) {
	case -1:
		return -1;
	case 0:
		if (input)
			dup2(pifds[Wr], STDOUT_FILENO);
		else
			dup2(pifds[Rd], STDIN_FILENO);
		close(pifds[input? Rd: Wr]);
		dup2(fd, (input? STDIN_FILENO: STDOUT_FILENO));
		if (!strcmp(cmd, "zcat")) {
			__progname = cmd;
			exit(cat_main(1, (char*[]){cmd}));
		}
		if (!strcmp(cmd, "gzip"))
			exit(gzip_main(1, (char*[]){cmd}));
		execlp(cmd, cmd, NULL);
		err(2, "can't exec %s", cmd);
	default:
		nfd = pifds[input? Rd: Wr];
		close(pifds[input? Wr: Rd]);
		break;
	}
	ps->rfd = nfd;
	ps->open = 1;
	return nfd;
}

static int
pushclose(Pushstate *ps)
{
	int status;

	if (ps->fd < 0 || ps->rfd < 0 || !ps->open)
		return 2;
	close(ps->rfd);
	ps->rfd = -1;
	ps->open = 0;
	waitpid(ps->kid, &status, 0);
	return WEXITSTATUS(status) ? 2 : 0;
}

/*
 * block-buffer management
 */

static void
initblks(void)
{
	free(tpblk);
	tpblk = malloc(Tblock * nblock);
	assert(tpblk != NULL);
	endblk = tpblk + nblock;
}

/*
 * (re)fill block buffers from archive.  `justhdr' means we don't care
 * about the data before the next header block.
 */
static char *
refill(int ar, char *bufs, int justhdr)
{
	int i;
	unsigned n;
	unsigned bytes = Tblock * nblock;
	static int done, first = 1, seekable;

	if (done)
		return NULL;

	blkoff = lseek(ar, 0, SEEK_CUR);		/* note position for `tar r' */
	if (first)
		seekable = blkoff >= 0;
	/* try to size non-pipe input at first read */
	if (first && usefile) {
		n = eread(arname, ar, bufs, bytes);
		if (n == 0)
			err(2, "EOF reading archive %s", arname);
		i = n;
		if (i % Tblock != 0)
			errx(2, "%s: archive block size (%d) error", arname, i);
		i /= Tblock;
		if (i != nblock) {
			nblock = i;
			warnx("blocking = %d", nblock);
			endblk = (Hdr *)bufs + nblock;
			bytes = n;
		}
	} else if (justhdr && seekable && nexthdr - blkoff >= bytes) {
		/* optimisation for huge archive members on seekable media */
		if (lseek(ar, bytes, SEEK_CUR) < 0)
			err(2, "can't seek on archive");
		n = bytes;
	} else
		n = ereadn(arname, ar, bufs, bytes);
	first = 0;

	if (n == 0)
		errx(2, "unexpected EOF reading archive %s", arname);
	if (n % Tblock != 0)
		errx(2, "partial block read from archive %s", arname);
	if (n != bytes) {
		done = 1;
		memset(bufs + n, 0, bytes - n);
	}
	return bufs;
}

static Hdr *
getblk(int ar, Refill rfp, int justhdr)
{
	if (curblk == NULL || curblk >= endblk) {  /* input block exhausted? */
		if (rfp != NULL && (*rfp)(ar, (char *)tpblk, justhdr) == NULL)
			return NULL;
		curblk = tpblk;
	}
	return curblk++;
}

static Hdr *
getblkrd(int ar, int justhdr)
{
	return getblk(ar, refill, justhdr);
}

static Hdr *
getblke(int ar)
{
	return getblk(ar, NULL, Alldata);
}

static Hdr *
getblkz(int ar)
{
	Hdr *hp = getblke(ar);

	if (hp != NULL)
		memset(hp->data, 0, Tblock);
	return hp;
}

/*
 * how many block buffers are available, starting at the address
 * just returned by getblk*?
 */
static int
gothowmany(int max)
{
	int n = endblk - (curblk - 1);

	return n > max? max: n;
}

/*
 * indicate that one is done with the last block obtained from getblke
 * and it is now available to be written into the archive.
 */
static void
putlastblk(int ar)
{
	unsigned bytes = Tblock * nblock;

	/* if writing end-of-archive, aid compression (good hygiene too) */
	if (curblk < endblk)
		memset(curblk, 0, (char *)endblk - (char *)curblk);
	if (write(ar, tpblk, bytes) != bytes)
		err(2, "error writing archive");
}

static void
putblk(int ar)
{
	if (curblk >= endblk)
		putlastblk(ar);
}

static void
putbackblk(int ar)
{
	curblk--;
	(void)ar;
}

static void
putreadblks(int ar, int blks)
{
	curblk += blks - 1;
	(void)ar;
}

static void
putblkmany(int ar, int blks)
{
	assert(blks > 0);
	curblk += blks - 1;
	putblk(ar);
}

/*
 * common routines
 */

/*
 * modifies hp->chksum but restores it; important for the last block of the
 * old archive when updating with `tar rf archive'
 */
static long
chksum(Hdr *hp)
{
	int n = Tblock;
	long i = 0;
	unsigned char *cp = hp->data;
	char oldsum[sizeof hp->chksum];

	memmove(oldsum, hp->chksum, sizeof oldsum);
	memset(hp->chksum, ' ', sizeof hp->chksum);
	while (n-- > 0)
		i += *cp++;
	memmove(hp->chksum, oldsum, sizeof oldsum);
	return i;
}

static int
isustar(Hdr *hp)
{
	return strcmp(hp->magic, "ustar") == 0;
}

/*
 * s is at most n bytes long, but need not be NUL-terminated.
 * if shorter than n bytes, all bytes after the first NUL must also
 * be NUL.
 */
static int
tar_sstrnlen(char *s, size_t n)
{
	return s[n - 1] != '\0'? n: strlen(s);
}

/* set fullname from header */
static char *
name(Hdr *hp)
{
	int pfxlen, namlen;
	char *fullname;
	static char fullnamebuf[2+Maxname+1];  /* 2+ for ./ on relative names */

	fullname = fullnamebuf+2;
	namlen = tar_sstrnlen(hp->name, sizeof hp->name);
	if (hp->prefix[0] == '\0' || !isustar(hp)) {	/* old-style name? */
		memmove(fullname, hp->name, namlen);
		fullname[namlen] = '\0';
		return fullname;
	}

	/* name is in two pieces */
	pfxlen = tar_sstrnlen(hp->prefix, sizeof hp->prefix);
	memmove(fullname, hp->prefix, pfxlen);
	fullname[pfxlen] = '/';
	memmove(fullname + pfxlen + 1, hp->name, namlen);
	fullname[pfxlen + 1 + namlen] = '\0';
	return fullname;
}

static int
isdir(Hdr *hp)
{
	/* the mode test is ugly but sometimes necessary */
	return hp->linkflag == LF_DIR ||
		strrchr(name(hp), '\0')[-1] == '/' ||
		(strtoul(hp->mode, NULL, 8)&0170000) == 040000;
}

static int
eotar(Hdr *hp)
{
	return name(hp)[0] == '\0';
}

/*
static uvlong
getbe(unsigned char *src, int size)
{
	uvlong vl = 0;

	while (size-- > 0) {
		vl <<= 8;
		vl |= *src++;
	}
	return vl;
}
 */

static void
putbe(unsigned char *dest, unsigned long long vl, int size)
{
	for (dest += size; size-- > 0; vl >>= 8)
		*--dest = vl;
}

/*
 * cautious parsing of octal numbers as ascii strings in
 * a tar header block.  this is particularly important for
 * trusting the checksum when trying to resync.
 */
static unsigned long long
hdrotoull(char *st, char *end, unsigned long long errval, char *name, char *field)
{
	char *numb;

	for (numb = st; (*numb == ' ' || *numb == '\0') && numb < end; numb++)
		;
	if (numb < end && isascii(*numb) && isdigit(*numb))
		return strtoull(numb, NULL, 8);
	else if (numb >= end)
		warnx("%s: empty %s in header", name, field);
	else
		warnx("%s: %s: non-numeric %s in header",
			name, numb, field);
	return errval;
}

/*
 * return the nominal size from the header block, which is not always the
 * size in the archive (the archive size may be zero for some file types
 * regardless of the nominal size).
 *
 * gnu and freebsd tars are now recording vlongs as big-endian binary
 * with a flag in byte 0 to indicate this, which permits file sizes up to
 * 2^64-1 (actually 2^80-1 but our file sizes are vlongs) rather than 2^33-1.
 */
static Off
hdrsize(Hdr *hp)
{
	unsigned char *p;

	if((unsigned char)hp->size[0] == Binnegsz) {
		warnx("%s: negative length, which is insane",
			name(hp));
		return 0;
	} else if((unsigned char)hp->size[0] == Binsize) {
		p = (unsigned char *)hp->size + sizeof hp->size - 1 -
			sizeof(long long);		/* -1 for terminating space */
		return G8BEBYTE(p);
	}

	return hdrotoull(hp->size, hp->size + sizeof hp->size, 0,
		name(hp), "size");
}

/*
 * return the number of bytes recorded in the archive.
 */
static Off
arsize(Hdr *hp)
{
	if(isdir(hp) || islink(hp->linkflag))
		return 0;
	return hdrsize(hp);
}
static long
parsecksum(char *cksum, char *name)
{
	return hdrotoull(cksum, cksum + 8, (unsigned long long)-1LL,
		name, "checksum");
}

static Hdr *
readhdr(int ar)
{
	long hdrcksum;
	Hdr *hp;

	hp = getblkrd(ar, Alldata);
	if (hp == NULL)
		errx(2, "unexpected EOF instead of archive header in %s",
			arname);
	if (eotar(hp))			/* end-of-archive block? */
		return NULL;

	hdrcksum = parsecksum(hp->chksum, name(hp));
	if (hdrcksum == -1 || chksum(hp) != hdrcksum) {
		if (!resync)
			errx(2, "bad archive header checksum in %s: "
				"name %.100s...; expected %lo got %lo",
				arname, hp->name, hdrcksum, chksum(hp));
		fprintf(stderr, "skipping past archive header with bad checksum in %s...",
			arname);
		do {
			hp = getblkrd(ar, Alldata);
			if (hp == NULL)
				errx(2, "unexpected EOF looking for archive header in %s",
					arname);
			hdrcksum = parsecksum(hp->chksum, name(hp));
		} while (hdrcksum == -1 || chksum(hp) != hdrcksum);
		fprintf(stderr, "found %s\n", name(hp));
	}
	nexthdr += Tblock*(1 + BYTES2TBLKS(arsize(hp)));
	return hp;
}

/*
 * tar r[c]
 */

/*
 * if name is longer than Namsiz bytes, try to split it at a slash and fit the
 * pieces into hp->prefix and hp->name.
 */
static int
putfullname(Hdr *hp, char *name)
{
	size_t namlen, pfxlen;
	char *sl, *osl;

	namlen = strlen(name);
	if (isdir(hp) && name[namlen-1] != '/')
		namlen++;
	if (namlen <= Namsiz) {
		strncpy(hp->name, name, Namsiz);
		if (hp->name[namlen-1] == 0)
			hp->name[namlen-1] = '/';
		hp->prefix[0] = '\0';		/* ustar paranoia */
		return 0;
	}

	if (!posix || namlen > Maxname) {
		warnx("name too long for tar header: %s", name);
		return -1;
	}
	/*
	 * try various splits until one results in pieces that fit into the
	 * appropriate fields of the header.  look for slashes from right
	 * to left, in the hopes of putting the largest part of the name into
	 * hp->prefix, which is larger than hp->name.
	 */
	sl = strrchr(name, '/');
	while (sl != NULL) {
		pfxlen = sl - name;
		if (pfxlen <= sizeof hp->prefix && namlen-1 - pfxlen <= Namsiz)
			break;
		osl = sl;
		*osl = '\0';
		sl = strrchr(name, '/');
		*osl = '/';
	}
	if (sl == NULL) {
		warnx("name can't be split to fit tar header: %s", name);
		return -1;
	}
	*sl = '\0';
	strncpy(hp->prefix, name, sizeof hp->prefix-1);
	hp->prefix[sizeof hp->prefix-1] = 0;
	*sl++ = '/';
	strncpy(hp->name, sl, sizeof hp->name);
	if (hp->name[namlen-pfxlen-2] == 0)
		hp->name[namlen-pfxlen-2] = '/';
	return 0;
}

static int
mkhdr(Hdr *hp, struct stat *dir, char *file, const char *shortf)
{
	int r;

	/*
	 * some of these fields run together, so we format them left-to-right
	 * and don't use snprint.
	 */
	sprintf(hp->mode, "%6lo ", (unsigned long)dir->st_mode & 0777);
	sprintf(hp->uid, "%6o ", dir->st_uid);
	sprintf(hp->gid, "%6o ", dir->st_gid);
	if (dir->st_size >= (Off)1<<32) {
		static int printed;

		if (!printed) {
			printed = 1;
			warnx("storing large sizes in \"base 256\"");
		}
		hp->size[0] = (char)Binsize;
		/* emit so-called `base 256' representation of size */
		putbe((unsigned char *)hp->size+1, dir->st_size, sizeof hp->size - 2);
		hp->size[sizeof hp->size - 1] = ' ';
	} else
		sprintf(hp->size, "%11llo ", (unsigned long long)dir->st_size);
	sprintf(hp->mtime, "%11llo ", (unsigned long long)dir->st_mtime);
	if (S_ISDIR(dir->st_mode))
		hp->linkflag = LF_DIR;
	else if (S_ISLNK(dir->st_mode)) {
		char buf[PATH_MAX];
		ssize_t len;

		hp->linkflag = LF_SYMLINK1;
		len = readlink(shortf, buf, sizeof buf-1);
		if (len != -1)
			memcpy(hp->linkname, buf, len);
	}
	else if (S_ISCHR(dir->st_mode))
		hp->linkflag = LF_CHR;
	else if (S_ISBLK(dir->st_mode))
		hp->linkflag = LF_BLK;
	else if (S_ISFIFO(dir->st_mode))
		hp->linkflag = LF_FIFO;
	else
		hp->linkflag = LF_PLAIN1;
	r = putfullname(hp, file);
	if (posix) {
		struct passwd *pw;
		struct group *gr;

		pw = getpwuid(dir->st_uid);
		gr = getgrgid(dir->st_gid);
		memcpy(hp->magic, "ustar", sizeof hp->magic);
		memcpy(hp->version, "00", sizeof hp->version);
		if (pw) {
			strncpy(hp->uname, pw->pw_name, sizeof hp->uname-1);
			hp->uname[sizeof hp->uname-1] = 0;
		}
		if (gr) {
			strncpy(hp->gname, gr->gr_name, sizeof hp->gname-1);
			hp->gname[sizeof hp->gname-1] = 0;
		}
	}
	sprintf(hp->chksum, "%6lo", chksum(hp));
	return r;
}

static void addtoar(int ar, char *file, char *shortf);

static void
addtreetoar(int ar, char *file, char *shortf, int fd)
{
	DIR *d;
	struct dirent *de;
	char *name = NULL;

	d = fdopendir(fd);
	if (!d)
		return;

	if (chdir(shortf) < 0)
		err(2, "chdir %s", file);
	if (Debug)
		fprintf(stderr, "chdir %s\t# %s\n", shortf, file);

	while ((de = readdir(d)) != NULL) {
		if (de->d_name[0] == '.' && (de->d_name[1] == 0 || (de->d_name[1] == '.' && de->d_name[2] == 0)))
			continue;
		name = realloc(name, strlen(file) + strlen(de->d_name) + 2);
		strcpy(name, file);
		strcat(name, "/");
		strcat(name, de->d_name);
		addtoar(ar, name, de->d_name);
	}
	free(name);
	closedir(d);

	/*
	 * this assumes that shortf is just one component, which is true
	 * during directory descent, but not necessarily true of command-line
	 * arguments.  Our caller (or addtoar's) must reset the working
	 * directory if necessary.
	 */
	if (chdir("..") < 0)
		err(2, "chdir %s/..", file);
	if (Debug)
		fprintf(stderr, "chdir ..\n");
}

static void
addtoar(int ar, char *file, char *shortf)
{
	int n, fd, isdir;
	long bytes, blksread;
	unsigned long blksleft;
	Hdr *hbp;
	struct stat dir[1];
	char name[strlen(shortf) + 3];

	if (shortf[0] == '#') {
		strcpy(name, "./");
		strcat(name, shortf);
		shortf = name;
	}

	if (Debug)
		fprintf(stderr, "opening %s	# %s\n", shortf, file);
	if (lstat(shortf, dir))
		err(2, "can't fstat %s", file);

	hbp = getblkz(ar);
	isdir = S_ISDIR(dir->st_mode);
	if (mkhdr(hbp, dir, file, shortf) < 0) {
		putbackblk(ar);
		return;
	}
	putblk(ar);

	blksleft = BYTES2TBLKS(dir->st_size);

	if (S_ISLNK(dir->st_mode))
		fd = -1;
	else {
		fd = open(shortf, O_RDONLY);
		if (fd < 0) {
			warn("can't open %s", file);
			return;
		}
	}
	if (isdir)
		addtreetoar(ar, file, shortf, fd);
	else if (fd != -1) {
		for (; blksleft > 0; blksleft -= blksread) {
			hbp = getblke(ar);
			blksread = gothowmany(blksleft);
			assert(blksread >= 0);
			bytes = blksread * Tblock;
			n = ereadn(file, fd, hbp->data, bytes);
			assert(n >= 0);
			/*
			 * ignore EOF.  zero any partial block to aid
			 * compression and emergency recovery of data.
			 */
			if (n < Tblock)
				memset(hbp->data + n, 0, bytes - n);
			putblkmany(ar, blksread);
		}
		close(fd);
		if (verbose)
			fprintf(stderr, "%s\n", file);
	}
}

static void
skip(int ar, Hdr *hp, char *msg)
{
	unsigned long blksleft, blksread;
	Off bytes;

	bytes = arsize(hp);
	for (blksleft = BYTES2TBLKS(bytes); blksleft > 0; blksleft -= blksread) {
		if (getblkrd(ar, Justnxthdr) == NULL)
			errx(2, "unexpected EOF on archive %s %s", arname, msg);
		blksread = gothowmany(blksleft);
		putreadblks(ar, blksread);
	}
}

static void
skiptoend(int ar)
{
	Hdr *hp;

	while ((hp = readhdr(ar)) != NULL)
		skip(ar, hp, "skipping to end");

	/*
	 * we have just read the end-of-archive Tblock.
	 * now seek back over the (big) archive block containing it,
	 * and back up curblk ptr over end-of-archive Tblock in memory.
	 */
	if (lseek(ar, blkoff, 0) < 0)
		err(2, "can't seek back over end-of-archive in %s", arname);
	curblk--;
}

/*
 * In place, rewrite name to compress multiple /, eliminate ., and process ..
 */
#define SEP(x)	((x)=='/' || (x) == 0)
static char*
cleanname(char *name)
{
	char *p, *q, *dotdot;
	int rooted;

	rooted = name[0] == '/';

	/*
	 * invariants:
	 *	p points at beginning of path element we're considering.
	 *	q points just past the last path element we wrote (no slash).
	 *	dotdot points just past the point where .. cannot backtrack
	 *		any further (no slash).
	 */
	p = q = dotdot = name+rooted;
	while(*p) {
		if(p[0] == '/')	/* null element */
			p++;
		else if(p[0] == '.' && SEP(p[1]))
			p += 1;	/* don't count the separator in case it is nul */
		else if(p[0] == '.' && p[1] == '.' && SEP(p[2])) {
			p += 2;
			if(q > dotdot) {	/* can backtrack */
				while(--q > dotdot && *q != '/')
					;
			} else if(!rooted) {	/* /.. is / but ./../ is .. */
				if(q != name)
					*q++ = '/';
				*q++ = '.';
				*q++ = '.';
				dotdot = q;
			}
		} else {	/* real path element */
			if(q != name+rooted)
				*q++ = '/';
			while((*q = *p) != '/' && *q != 0)
				p++, q++;
		}
	}
	if(q == name)	/* empty string is really ``.'' */
		*q++ = '.';
	*q = '\0';
	return name;
}

static int
replace(char **argv)
{
	int i, ar;
	char *arg;
	Compress *comp = NULL;
	Pushstate ps;

	/* open archive to be updated */
	if (usefile && docreate)
		ar = creat(usefile, 0666);
	else if (usefile) {
		if (docompress)
			errx(2, "cannot update compressed archive");
		ar = open(usefile, O_RDWR);
	} else
		ar = STDOUT_FILENO;

	/* push compression filter, if requested */
	if (docompress) {
		comp = compmethod(usefile);
		if (comp)
			ar = push(ar, comp->comp, Output, &ps);
	}
	if (ar < 0)
		err(2, "can't open archive %s", usefile);

	if (usefile && !docreate)
		skiptoend(ar);

	for (i = 0; argv[i] != NULL; i++) {
		arg = argv[i];
		cleanname(arg);
		if (strcmp(arg, "..") == 0 || strncmp(arg, "../", 3) == 0)
			warnx("name starting with .. is a bad idea");
		addtoar(ar, arg, arg);
		chdir(origdir);		/* for correctness & profiling */
	}

	/* write end-of-archive marker */
	getblkz(ar);
	putblk(ar);
	getblkz(ar);
	putlastblk(ar);

	if (comp)
		return pushclose(&ps);
	if (ar > STDERR_FILENO)
		close(ar);
	return 0;
}

/*
 * tar [xt]
 */

/* is pfx a file-name prefix of name? */
static int
prefix(char *name, char *pfx)
{
	int pfxlen = strlen(pfx);
	char clpfx[Maxname+1];

	if (pfxlen > Maxname)
		return 0;
	strcpy(clpfx, pfx);
	cleanname(clpfx);
	return strncmp(clpfx, name, pfxlen) == 0 &&
		(name[pfxlen] == '\0' || name[pfxlen] == '/');
}

static int
match(char *name, char **argv)
{
	int i;
	char clname[Maxname+1];

	if (argv[0] == NULL)
		return 1;
	strcpy(clname, name);
	cleanname(clname);
	for (i = 0; argv[i] != NULL; i++)
		if (prefix(clname, argv[i]))
			return 1;
	return 0;
}

static void
cantcreate(char *s, int dir)
{
	int len;
	static char *last;

	/*
	 * Always print about files.  Only print about directories
	 * we haven't printed about.  (Assumes archive is ordered
	 * nicely.)
	 */
	if(dir){
		if(last){
			/* already printed this directory */
			if(strcmp(s, last) == 0)
				return;
			/* printed a higher directory, so printed this one */
			len = strlen(s);
			if(memcmp(s, last, len) == 0 && last[len] == '/')
				return;
		}
		/* save */
		free(last);
		last = strdup(s);
	}
	warn("can't create %s", s);
}

static int
makedir(char *s)
{
	int f;

	if (access(s, F_OK) == 0)
		return -1;
	f = mkdir(s, 0777);
	if (f == -1)
		cantcreate(s, 1);
	return f;
}

static int
mkpdirs(char *s)
{
	int err;
	char *p;

	p = s;
	err = 0;
	while (!err && (p = strchr(p+1, '/')) != NULL) {
		*p = '\0';
		err = (access(s, F_OK) < 0 && makedir(s) < 0);
		*p = '/';
	}
	return -err;
}

/* Call access but preserve the errno. */
static int
xaccess(char *name, int mode)
{
	int rv;
	int save_errno = errno;

	rv = access(name, mode);
	errno = save_errno;
	return rv;
}

static int
openfname(Hdr *hp, char *fname, int dir, int mode)
{
	int fd;

	fd = -1;
	cleanname(fname);
	switch (hp->linkflag) {
	case LF_LINK:
	case LF_SYMLINK1:
	case LF_SYMLINK2:
		warnx("can't make (sym)link %s", fname);
		break;
	case LF_FIFO:
		warnx("can't make fifo %s", fname);
		break;
	default:
		if (!keepexisting || access(fname, F_OK) < 0) {
			if (dir) {
				fd = mkdir(fname, mode);
				if (fd == -1) {
					mkpdirs(fname);
					fd = mkdir(fname, mode);
				}
				if (fd == 0)
					fd = open(fname, O_RDONLY, mode);
			}
			else {
				fd = creat(fname, mode);
				if (fd < 0) {
					mkpdirs(fname);
					fd = creat(fname, mode);
				}
			}
			if (fd < 0 && (!dir || xaccess(fname, F_OK) < 0))
				cantcreate(fname, dir);
		}
		if (fd >= 0 && verbose)
			fprintf(stderr, "%s\n", fname);
		break;
	}
	return fd;
}

/* copy from archive to file system (or nowhere for table-of-contents) */
static void
copyfromar(int ar, int fd, char *fname, unsigned long blksleft, Off bytes)
{
	int wrbytes;
	unsigned long blksread;
	Hdr *hbp;

	if (blksleft == 0)
		bytes = 0;
	for (; blksleft > 0; blksleft -= blksread) {
		hbp = getblkrd(ar, (fd >= 0? Alldata: Justnxthdr));
		if (hbp == NULL)
			errx(2, "unexpected EOF on archive extracting %s from %s",
				fname, arname);
		blksread = gothowmany(blksleft);
		if (blksread <= 0)
			warnx("got %ld blocks reading %s!",
				blksread, fname);
		wrbytes = Tblock*blksread;
		assert(bytes >= 0);
		if(wrbytes > bytes)
			wrbytes = bytes;
		if (fd >= 0 && write(fd, hbp, wrbytes) != wrbytes)
			err(2, "write error on %s", fname);
		putreadblks(ar, blksread);
		bytes -= wrbytes;
		assert(bytes >= 0);
	}
	if (bytes > 0)
		warnx("%lld bytes uncopied at EOF on archive %s; "
			"%s not fully extracted", bytes, arname, fname);
}

static void
wrmeta(int fd, Hdr *hp, long mtime, int mode)		/* update metadata */
{
	struct timespec times[2];

	times[0].tv_sec = mtime;
	times[0].tv_nsec = 0;
	times[1].tv_sec = mtime;
	times[1].tv_nsec = 0;
	futimens(fd, times);
	fchmod(fd, mode);
	if (isustar(hp)) {
		// TODO
	} else
		fchown(fd, atoi(hp->uid), atoi(hp->gid));
}

/*
 * copy a file from the archive into the filesystem.
 * fname is result of name(), so has two extra bytes at beginning.
 */
static void
extract1(int ar, Hdr *hp, char *fname)
{
	int fd = -1, dir = 0;
	time_t mtime = strtol(hp->mtime, NULL, 8);
	unsigned long mode = strtoul(hp->mode, NULL, 8) & 0777;
	Off bytes = hdrsize(hp);		/* for printing */
	unsigned long blksleft = BYTES2TBLKS(arsize(hp));

	/* fiddle name, figure out mode and blocks */
	if (isdir(hp)) {
		mode |= 0700;
		dir = 1;
	}
	switch (hp->linkflag) {
	case LF_LINK:
	case LF_SYMLINK1:
	case LF_SYMLINK2:
	case LF_FIFO:
		blksleft = 0;
		break;
	}
	if (relative) {
		if(fname[0] == '/')
			*--fname = '.';
		else if(fname[0] == '#'){
			*--fname = '/';
			*--fname = '.';
		}
	}
	if (verb == Xtract)
		fd = openfname(hp, fname, dir, mode);
	else if (verbose) {
		char *cp = ctime(&mtime);

		printf("%lo %8lld %-12.12s %-4.4s %s\n",
			mode, bytes, cp+4, cp+24, fname);
	} else
		printf("%s\n", fname);

	copyfromar(ar, fd, fname, blksleft, bytes);

	/* touch up meta data and close */
	if (fd >= 0) {
		/*
		 * directories should be wstated *after* we're done
		 * creating files in them, but we don't do that.
		 */
		if (settime)
			wrmeta(fd, hp, mtime, mode);
		close(fd);
	}
}

static int
extract(char **argv)
{
	int ar;
	char *longname;
	char msg[Maxname + 40] = "extracting ";
	Compress *comp = NULL;
	Hdr *hp;
	Pushstate ps;

	/* open archive to be read */
	if (usefile)
		ar = open(usefile, O_RDONLY);
	else
		ar = STDIN_FILENO;

	/* push decompression filter if requested or extension is known */
	comp = compmethod(usefile);
	if (comp)
		ar = push(ar, comp->decomp, Input, &ps);
	if (ar < 0)
		err(2, "can't open archive %s", usefile);

	while ((hp = readhdr(ar)) != NULL) {
		longname = name(hp);
		if (match(longname, argv))
			extract1(ar, hp, longname);
		else {
			strcat(msg, longname);
			skip(ar, hp, msg);
		}
	}

	if (comp)
		return pushclose(&ps);
	if (ar > STDERR_FILENO)
		close(ar);
	return 0;
}

int
tar_main(int argc, char *argv[])
{
	int errflg = 0;
	int ret = 0;

	static_assert(sizeof(Hdr) == Tblock, "");
	argv++, argc--;
	for (const char *_argc = *argv; *_argc; _argc++)
	switch (*_argc) {
	case 'c':
		docreate++;
		verb = Replace;
		break;
	case 'f':
		if (argc < 2)
			usage();
		usefile = arname = argv++[1];
		break;
	case 'i':
		ignerrs = 1;
		break;
	case 'k':
		keepexisting++;
		break;
	case 'm':	/* compatibility */
		settime = 0;
		break;
	case 'p':
		posix++;
		break;
	case 'P':
		posix = 0;
		break;
	case 'r':
		verb = Replace;
		break;
	case 'R':
		relative = 0;
		break;
	case 's':
		resync++;
		break;
	case 't':
		verb = Toc;
		break;
	case 'T':
		settime++;
		break;
	case 'v':
		verbose++;
		break;
	case 'x':
		verb = Xtract;
		break;
	case 'z':
		docompress++;
		break;
	case '-':
		break;
	default:
		warnx("unknown letter %c", *_argc);
		errflg++;
		break;
	}
	argc--, argv++;

	if (argc < 0 || errflg)
		usage();

	initblks();
	switch (verb) {
	case Toc:
	case Xtract:
		ret = extract(argv);
		break;
	case Replace:
		if (getcwd(origdir, sizeof origdir) == NULL)
			strcpy(origdir, "/tmp");
		ret = replace(argv);
		break;
	default:
		usage();
		break;
	}
	exit(ret);
}
