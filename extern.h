/*
 * Copyright (c) 2021 Matija Skala
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#pragma once

#include <stddef.h>

char *do_basename(const char *, const char *, size_t *);

int basename_main(int argc, char *argv[]);
int cat_main(int argc, char *argv[]);
int cp_main(int argc, char *argv[]);
int diff_main(int argc, char *argv[]);
int dirname_main(int argc, char *argv[]);
int gzip_main(int argc, char *argv[]);
int id_main(int argc, char *argv[]);
int kilo_main(int argc, char *argv[]);
int ls_main(int argc, char *argv[]);
int mv_main(int argc, char *argv[]);
int pwd_main(int argc, char *argv[]);
int realpath_main(int argc, char *argv[]);
int rm_main(int argc, char *argv[]);
int rmdir_main(int argc, char *argv[]);
int sed_main(int argc, char *argv[]);
int sh_main(int argc, char *argv[]);
int sum_main(int argc, char *argv[]);
int tar_main(int argc, char *argv[]);
int test_main(int argc, char *argv[]);
