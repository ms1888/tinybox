/*-
 * Copyright (c) 1991, 1993
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * James W. Williams of NASA Goddard Space Flight Center.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#define _POSIX_C_SOURCE 2

#include "extern.h"

#include <sys/types.h>

#include <err.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void
usage(void)
{
	fprintf(stderr, "usage: sum [-rs] [file ...]\n");
	exit(1);
}

static int
csum(int fd, uint32_t *cval, off_t *clen, int rflag)
{
	uint32_t lcrc;
	int nr;
	off_t total;
	unsigned char *p;
	unsigned char buf[8192];

	/*
	 * Draft 8 POSIX 1003.2:
	 *
	 *   s = sum of all bytes
	 *   r = s % 2^16 + (s % 2^32) / 2^16
	 * lcrc = (r % 2^16) + r / 2^16
	 */
	lcrc = total = 0;
	while ((nr = read(fd, buf, sizeof(buf))) > 0) {
		if (rflag)
			for (total += nr, p = buf; nr--; ++p) {
				if (lcrc & 1)
					lcrc |= 0x10000;
				lcrc = ((lcrc >> 1) + *p) & 0xffff;
			}
		else
			for (total += nr, p = buf; nr--; ++p)
				lcrc += *p;
	}

	if (!rflag) {
		lcrc = (lcrc & 0xffff) + (lcrc >> 16);
		lcrc = (lcrc & 0xffff) + (lcrc >> 16);
	}

	*cval = lcrc;
	*clen = total;
	return EXIT_SUCCESS;
}

static void
psum(char *fn, uint32_t val, off_t len, int rflag)
{
	int unit = rflag ? 1024 : 512;
	printf("%lu %lld", (unsigned long)val, (long long)(len + unit - 1) / unit);
	if (fn != NULL)
		printf(" %s", fn);
	printf("\n");
}

int
sum_main(int argc, char *argv[])
{
	uint32_t val;
	int ch, fd, rval;
	off_t len;
	char *fn, *p;
	int rflag = 1;

	if ((p = strrchr(argv[0], '/')) == NULL)
		p = argv[0];
	else
		++p;
	while ((ch = getopt(argc, argv, "rs")) != -1)
		switch (ch) {
			case 'r':
				rflag = 1;
				break;
			case 's':
				rflag = 0;
				break;
			default:
				usage();
		}
	argv += optind;

	fd = STDIN_FILENO;
	fn = NULL;
	rval = 0;
	do {
		if (*argv) {
			fn = *argv++;
			if ((fd = open(fn, O_RDONLY, 0)) < 0) {
				warn("%s", fn);
				rval = 1;
				continue;
			}
		}
		if (csum(fd, &val, &len, rflag)) {
			warn("%s", fn ? fn : "stdin");
			rval = 1;
		} else
			psum(fn, val, len, rflag);
		close(fd);
	} while (*argv);
	exit(rval);
}
