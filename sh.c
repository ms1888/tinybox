/*
 * Copyright (c) 1987,1997,2001 Prentice Hall
 * All rights reserved.
 *
 * Redistribution and use of the MINIX operating system in source and
 * binary forms, with or without modification, are permitted provided
 * that the following conditions are met:
 *
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *    * Neither the name of Prentice Hall nor the names of the software
 *      authors or contributors may be used to endorse or promote
 *      products derived from this software without specific prior
 *      written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS, AUTHORS, AND
 * CONTRIBUTORS ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL PRENTICE HALL OR ANY AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _DEFAULT_SOURCE

#include "extern.h"

#include <dirent.h>
#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <paths.h>
#include <setjmp.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/wait.h>
#include <unistd.h>

/* -------- sh.h -------- */
/*
 * shell
 */

#define	LINELIM	4096
#define	NPUSH	8	/* limit to input nesting */

#define	NOFILE_	20	/* Number of open files */
#define	NUFILE	10	/* Number of user-accessible files */
#define	FDBASE	10	/* First file usable by Shell */

/*
 * library and system defintions
 */
typedef void xint;	/* base type of jmp_buf, for not broken compilers */

/*
 * shell components
 */
/* #include "area.h" */
/* #include "word.h" */
/* #include "io.h" */
/* #include "var.h" */

#define	QUOTE	0200

#define	NOBLOCK	((struct op *)NULL)
#define	NOWORD	((char *)NULL)
#define	NOWORDS	((char **)NULL)
#define	NOPIPE	((int *)NULL)

/*
 * Description of a command or an operation on commands.
 * Might eventually use a union.
 */
struct op {
	int	type;	/* operation type, see below */
	char	**words;	/* arguments to a command */
	struct	ioword	**ioact;	/* IO actions (eg, < > >>) */
	struct op *left;
	struct op *right;
	char	*str;	/* identifier for case and for */
};

#define	TCOM	1	/* command */
#define	TPAREN	2	/* (c-list) */
#define	TPIPE	3	/* a | b */
#define	TLIST	4	/* a [&;] b */
#define	TOR	5	/* || */
#define	TAND	6	/* && */
#define	TFOR	7
#define	TDO	8
#define	TCASE	9
#define	TIF	10
#define	TWHILE	11
#define	TUNTIL	12
#define	TELIF	13
#define	TPAT	14	/* pattern in case */
#define	TBRACE	15	/* {c-list} */
#define	TASYNC	16	/* c & */

/*
 * actions determining the environment of a process
 */
#define	BIT(i)	(1<<(i))
#define	FEXEC	BIT(0)	/* execute without forking */

/*
 * flags to control evaluation of words
 */
#define	DOSUB	1	/* interpret $, `, and quotes */
#define	DOBLANK	2	/* perform blank interpretation */
#define	DOGLOB	4	/* interpret [?* */
#define	DOKEY	8	/* move words with `=' to 2nd arg. list */
#define	DOTRIM	16	/* trim resulting string */

#define	DOALL	(DOSUB|DOBLANK|DOGLOB|DOKEY|DOTRIM)

static	char	**dolv;
static	int	dolc;
static	int	exstat;
static  char	gflg;
static  int	talking;	/* interactive (talking-type wireless) */
static  int	execflg;
static  int	multiline;	/* \n changed to ; */
static  struct	op	*outtree;	/* result from parser */

static	xint	*failpt;
static	xint	*errpt;

struct	brkcon {
	jmp_buf	brkpt;
	struct	brkcon	*nextlev;
} ;
static	struct brkcon	*brklist;
static	int	isbreak;

/*
 * redirection
 */
struct ioword {
	short	io_unit;	/* unit affected */
	short	io_flag;	/* action (below) */
	char	*io_name;	/* file name */
	int	io_fd;
};
#define	IOREAD	1	/* < */
#define	IOHERE	2	/* << (here file) */
#define	IOWRITE	4	/* > */
#define	IOCAT	8	/* >> */
#define	IOXHERE	16	/* ${}, ` in << */
#define	IODUP	32	/* >&digit */
#define	IOCLOSE	64	/* >&- */

#define	IODEFAULT (-1)	/* token for default IO unit */

static	struct	wdblock	*wdlist;
static	struct	wdblock	*iolist;

/*
 * parsing & execution environment
 */
struct	env {
	char	*linep;
	struct	io	*iobase;
	struct	io	*iop;
	xint	*errpt;
	int	iofd;
	struct	env	*oenv;
};

/*
 * here documents
 */

struct	here {
	char	*h_tag;
	int	h_dosub;
	struct	ioword *h_iop;
	struct	here	*h_next;
	int	area;
};

static	char	*trap[_NSIG+1] = {0};
static	char	ourtrap[_NSIG+1] = {0};
static	int	trapset = 0;	/* trap pending */

static int     yynerrs;        /* yacc */

static	char	line[LINELIM];

/*
 * other functions
 */
static int (*inbuilt(char *s ))(struct op *);
static char *rexecve  (char *c , char **v , char **envp );
static char *space  (int n );
static char *strsave  (char *s , int a );
static char *evalstr  (char *cp , int f );
static char *putn  (int n );
static char *itoa  (unsigned u , int n );
static char *unquote  (char *as );
static struct var *lookup  (char *n );
static int rlookup  (char *n );
static struct wdblock *glob  (char *cp , struct wdblock *wb );
static int subgetc  (int ec , int quoted );
static char **makenv  (void);
static char **eval  (char **ap , int f );
static int setstatus  (int s );
static int waitfor  (int lastpid , int canintr );

static void onintr  (int s ); /* SIGINT handler */

static int newenv  (int f );
static void quitenv  (void);
static void sh_errf(const char *fmt, ...);
static void next  (int f );
static void setdash  (void);
static void onecommand  (void);
static void runtrap  (int i );
static int letter  (int c );
static int digit  (int c );
static int letnum  (int c );
static int gmatch  (char *s , char *p );

/*
 * error handling
 */
_Noreturn static void leave (void); /* abort shell (or fail in subshell) */
static void fail  (void);	 /* fail but return to process next command */
static void sig  (int i );	 /* default signal handler */

/* -------- var.h -------- */

struct	var {
	char	*value;
	char	*name;
	struct	var	*next;
	char	status;
};
#define	COPYV	1	/* flag to setval, suggesting copy */
#define	RONLY	01	/* variable is read-only */
#define	EXPORT	02	/* variable is to be exported */
#define	GETCELL	04	/* name & value space was got with getcell */

static	struct	var	*vlist;		/* dictionary */

static	struct	var	*homedir;	/* home directory */
static	struct	var	*prompt;	/* main prompt */
static	struct	var	*cprompt;	/* continuation prompt */
static	struct	var	*path;		/* search path for commands */
static	struct	var	*shell;		/* shell to interpret command files */
static	struct	var	*ifs;		/* field separators */

static int yyparse  (void);
static struct var *lookup  (char *n );
static void setval  (struct var *vp , char *val );
static void nameval  (struct var *vp , char *val , char *name );
static void export  (struct var *vp );
static void ronly  (struct var *vp );
static int isassign  (char *s );
static int checkname  (char *cp );
static int assign  (char *s , int cf );
static void putvlist  (int f );

static int execute  (struct op *t , int *pin , int *pout , int act );

/* -------- io.h -------- */
/* io buffer */
struct iobuf {
  unsigned id;				/* buffer id */
  char buf[512];			/* buffer */
  char *bufp;				/* pointer into buffer */
  char *ebufp;				/* pointer to end of buffer */
};

/* possible arguments to an IO function */
struct ioarg {
	char	*aword;
	char	**awordlist;
	int	afile;		/* file descriptor */
	unsigned afid;		/* buffer id */
	long	afpos;		/* file position */
	struct iobuf *afbuf;	/* buffer for this file */
};
static struct ioarg ioargstack[NPUSH];
#define AFID_NOBUF	(~0u)
#define AFID_ID		0u

/* an input generator's state */
struct	io {
	int	(*iofn)(struct ioarg *, struct io *);
	struct	ioarg	*argp;
	int	peekc;
	char	prev;		/* previous character read by readc() */
	char	nlcount;	/* for `'s */
	char	xchar;		/* for `'s */
	char	task;		/* reason for pushed IO */
};
static	struct	io	iostack[NPUSH];
#define	XOTHER	0	/* none of the below */
#define	XDOLL	1	/* expanding ${} */
#define	XGRAVE	2	/* expanding `'s */
#define	XIO	3	/* file IO */

/* in substitution */
#define	INSUB()	(e.iop->task == XGRAVE || e.iop->task == XDOLL)

/*
 * input generators for IO structure
 */
static int nlchar  (struct ioarg *ap, struct io *iop );
static int strchar  (struct ioarg *ap, struct io *iop );
static int qstrchar  (struct ioarg *ap, struct io *iop );
static int filechar  (struct ioarg *ap, struct io *iop );
static int herechar  (struct ioarg *ap, struct io *iop );
static int linechar  (struct ioarg *ap, struct io *iop );
static int gravechar  (struct ioarg *ap , struct io *iop );
static int qgravechar  (struct ioarg *ap , struct io *iop );
static int dolchar  (struct ioarg *ap, struct io *iop );
static int wdchar  (struct ioarg *ap, struct io *iop );
static void scraphere  (void);
static void freehere  (int area );
static void gethere  (void);
static void markhere  (char *s , struct ioword *iop );
static int herein  (char *hf, int xdoll);
static int run  (struct ioarg *argp , int (*f)(struct ioarg *, struct io *));

/*
 * IO functions
 */
static int eofc  (void);
static int mygetc  (int ec );
static int readc  (void);
static void unget  (int c );
static void ioecho  (int c );
static void prs  (char *s );
static void closef  (int i );
static void closeall  (void);

/*
 * IO control
 */
static void pushio  (struct ioarg *argp , int (*fn)(struct ioarg *, struct io *));
static int remap  (int fd );
static int openpipe  (int *pv );
static void closepipe  (int *pv );
static struct io *setbase  (struct io *ip );

#define	PUSHIO(what,arg,gen) ((temparg.what = (arg)),pushio(&temparg,(gen)))
#define	RUN(what,arg,gen) ((temparg.what = (arg)), run(&temparg,(gen)))

/* -------- word.h -------- */
#ifndef WORD_H
#define	WORD_H	1
struct	wdblock {
	short	w_bsize;
	short	w_nword;
	/* bounds are arbitrary */
	char	*w_words[1];
};

static struct wdblock *addword  (char *wd , struct wdblock *wb );
static struct wdblock *newword  (int nw );
static char **getwords  (struct wdblock *wb );
#endif

/* -------- area.h -------- */

/*
 * storage allocation
 */
static void setarea  (char *cp , int a );
static int getarea  (char *cp );

static	int	areanum;	/* current allocation area */

static void inithere(void);

/* -------- sh.c -------- */
/*
 * shell
 */

static struct ioarg temparg = {0, 0, 0, AFID_NOBUF, 0};

static int	intr;
static int	inparse;
/*
 * flags:
 * -e: quit on error
 * -k: look for name=value everywhere on command line
 * -n: no execution
 * -t: exit after reading and executing one command
 * -v: echo as read
 * -x: trace
 * -u: unset variables net diagnostic
 */
static char	flags['z'-'a'+1];
static char	*flag = &flags[0]-'a';
static char	*elinep = line+sizeof(line)-5;
static char	*const null	= "";
static bool	heedint =1;
static struct	env	e ={line, &iostack[0], &iostack[0]-1,
		    (xint *)NULL, FDBASE, (struct env *)NULL};

extern	char	**environ;	/* environment pointer */

/*
 * default shell, search rules
 */
static char	shellname[] = _PATH_BSHELL;
static char	search[] = ":/bin:/usr/bin";

static void (*qflag) (int) = SIG_IGN;

static int newfile (char *s );
static char *cclass (char *p, int sub );

int
sh_main(int argc, char *argv[])
{
	int f;
	char *s;
	int cflag;
	char *name, **ap;
	int (*iof)();

	if ((ap = environ) != NULL) {
		while (*ap)
			assign(*ap++, !COPYV);
		for (ap = environ; *ap;)
			export(lookup(*ap++));
	}
	closeall();
	areanum = 1;

	shell = lookup("SHELL");
	if (shell->value == null)
		setval(shell, shellname);
	export(shell);

	homedir = lookup("HOME");
	if (homedir->value == null)
		setval(homedir, "/");
	export(homedir);

	setval(lookup("$"), itoa(getpid(), 5));

	path = lookup("PATH");
	if (path->value == null)
		setval(path, search);
	export(path);

	ifs = lookup("IFS");
	if (ifs->value == null)
		setval(ifs, " \t\n");

	prompt = lookup("PS1");
	if (prompt->value == null)
#ifndef UNIXSHELL
		setval(prompt, "$ ");
#else
		setval(prompt, "% ");
#endif

	if (geteuid() == 0) {
		setval(prompt, "# ");
		prompt->status &= ~EXPORT;
	}
	cprompt = lookup("PS2");
	if (cprompt->value == null)
		setval(cprompt, "> ");

	iof = filechar;
	cflag = 0;
	name = *argv++;
	if (--argc >= 1) {
		if(argv[0][0] == '-' && argv[0][1] != '\0') {
			for (s = argv[0]+1; *s; s++)
				switch (*s) {
				case 'c':
					prompt->status &= ~EXPORT;
					cprompt->status &= ~EXPORT;
					setval(prompt, "");
					setval(cprompt, "");
					cflag = 1;
					if (--argc > 0)
						PUSHIO(aword, *++argv, iof = nlchar);
					break;
	
				case 'q':
					qflag = SIG_DFL;
					break;

				case 's':
					/* standard input */
					break;

				case 't':
					prompt->status &= ~EXPORT;
					setval(prompt, "");
					iof = linechar;
					break;
	
				case 'i':
					talking++;
				default:
					if (*s>='a' && *s<='z')
						flag[(int)*s]++;
				}
		} else {
			argv--;
			argc++;
		}
		if (iof == filechar && --argc > 0) {
			setval(prompt, "");
			setval(cprompt, "");
			prompt->status &= ~EXPORT;
			cprompt->status &= ~EXPORT;
			if (newfile(name = *++argv))
				exit(1);
		}
	}
	setdash();
	if (e.iop < iostack) {
		PUSHIO(afile, 0, iof);
		if (isatty(0) && isatty(1) && !cflag)
			talking++;
	}
	signal(SIGQUIT, qflag);
	if (name && name[0] == '-') {
		talking++;
		if ((f = open(".profile", 0)) >= 0)
			next(remap(f));
		if ((f = open("/etc/profile", 0)) >= 0)
			next(remap(f));
	}
	if (talking)
		signal(SIGTERM, sig);
	if (signal(SIGINT, SIG_IGN) != SIG_IGN)
		signal(SIGINT, onintr);
	dolv = argv;
	dolc = argc;
	dolv[0] = name;
	if (dolc > 1)
		for (ap = ++argv; --argc > 0;) {
			if (assign(*ap = *argv++, !COPYV))
				dolc--;	/* keyword */
			else
				ap++;
		}
	setval(lookup("#"), putn((--dolc < 0) ? (dolc = 0) : dolc));

	for (;;) {
		if (talking && e.iop <= iostack)
			prs(prompt->value);
		onecommand();
	}
}

static void
setdash(void)
{
	char *cp, c;
	char m['z'-'a'+1];

	cp = m;
	for (c='a'; c<='z'; c++)
		if (flag[(int)c])
			*cp++ = c;
	*cp = 0;
	setval(lookup("-"), m);
}

static int
newfile(char *s)
{
	int f;

	if (strcmp(s, "-") != 0) {
		f = open(s, 0);
		if (f < 0) {
			sh_errf("%s: cannot open\n", s);
			return(1);
		}
	} else
		f = 0;
	next(remap(f));
	return(0);
}

static void
onecommand(void)
{
	int i;
	jmp_buf m1;

	while (e.oenv)
		quitenv();
	areanum = 1;
	freehere(areanum);
	wdlist = 0;
	iolist = 0;
	e.errpt = 0;
	e.linep = line;
	yynerrs = 0;
	multiline = 0;
	inparse = 1;
	intr = 0;
	execflg = 0;
	setjmp(failpt = m1);	/* Bruce Evans' fix */
	if (setjmp(failpt = m1) || yyparse() || intr) {
		while (e.oenv)
			quitenv();
		scraphere();
		if (!talking && intr)
			leave();
		inparse = 0;
		intr = 0;
		return;
	}
	inparse = 0;
	brklist = 0;
	intr = 0;
	execflg = 0;
	if (!flag['n'])
		execute(outtree, NOPIPE, NOPIPE, 0);
	if (!talking && intr) {
		execflg = 0;
		leave();
	}
	if ((i = trapset) != 0) {
		trapset = 0;
		runtrap(i);
	}
}

static void
fail(void)
{
	longjmp(failpt, 1);
	/* NOTREACHED */
}

_Noreturn void
leave(void)
{
	if (execflg)
		fail();
	scraphere();
	freehere(1);
	runtrap(0);
	exit(exstat);
	/* NOTREACHED */
}

static void
sh_vwarnf(const char *fmt, va_list ap)
{
	vfprintf(stderr, fmt, ap);
	exstat = -1;
	if (flag['e'])
		leave();
}

static void
sh_warnf(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	sh_vwarnf(fmt, ap);
	va_end(ap);
}

static void
sh_errf(const char *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	sh_vwarnf(fmt, ap);
	va_end(ap);
	if (flag['n'])
		return;
	if (!talking)
		leave();
	if (e.errpt)
		longjmp(e.errpt, 1);
	closeall();
	e.iop = e.iobase = iostack;
}

static int
newenv(int f)
{
	struct env *ep;

	if (f) {
		quitenv();
		return(1);
	}
	ep = (struct env *) malloc(sizeof(*ep));
	if (ep == NULL) {
		while (e.oenv)
			quitenv();
		fail();
	}
	*ep = e;
	e.oenv = ep;
	e.errpt = errpt;
	return(0);
}

static void
quitenv(void)
{
	struct env *ep;
	int fd;

	if ((ep = e.oenv) != NULL) {
		fd = e.iofd;
		e = *ep;
		/* should close `'d files */
		free(ep);
		while (--fd >= e.iofd)
			close(fd);
	}
}

char *
putn(int n)
{
	return(itoa(n, -1));
}

char *
itoa(unsigned u, int n)
{
	char *cp;
	static char s[20];
	int m;

	m = 0;
	if (n < 0 && (int) u < 0) {
		m++;
		u = -u;
	}
	cp = s+sizeof(s);
	*--cp = 0;
	do {
		*--cp = u%10 + '0';
		u /= 10;
	} while (--n > 0 || u);
	if (m)
		*--cp = '-';
	return(cp);
}

static void
next(int f)
{
	PUSHIO(afile, f, filechar);
}

static void
onintr(int s)				/* ANSI C requires a parameter */
{
	signal(SIGINT, onintr);
	intr = 1;
	if (talking) {
		if (inparse) {
			prs("\n");
			fail();
		}
	}
	else if (heedint) {
		execflg = 0;
		leave();
	}
}

static int
letter(int c)
{
	return((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_');
}

static int
digit(int c)
{
	return(c >= '0' && c <= '9');
}

static int
letnum(int c)
{
	return(letter(c) || digit(c));
}

char *
space(int n)
{
	char *cp;

	if ((cp = malloc(n)) == 0)
		sh_errf("out of string space\n");
	return(cp);
}

char *
strsave(char *s, int a)
{
	char *cp;

	if ((cp = strdup(s)) != NULL) {
		setarea((char *)cp, a);
		return cp;
	}
	return("");
}

/*
 * trap handling
 */
static void
sig(int i)
{
	trapset = i;
	signal(i, sig);
}

void
runtrap(int i)
{
	char *trapstr;

	if ((trapstr = trap[i]) == NULL)
		return;
	if (i == 0)
		trap[i] = 0;
	RUN(aword, trapstr, nlchar);
}

/* -------- var.c -------- */

/*
 * Find the given name in the dictionary
 * and return its value.  If the name was
 * not previously there, enter it now and
 * return a null value.
 */
struct var *
lookup(char *n)
{
	struct var *vp;
	char *cp;
	int c;
	static struct var dummy;

	if (digit(*n)) {
		dummy.name = n;
		for (c = 0; digit(*n) && c < 1000; n++)
			c = c*10 + *n-'0';
		dummy.status = RONLY;
		dummy.value = c <= dolc? dolv[c]: null;
		return(&dummy);
	}
	cp = strchr(n, '=');
	if (!cp)
		cp = n + strlen(n);
	for (vp = vlist; vp; vp = vp->next)
		if (!strncmp(vp->name, n, cp - n) && (vp->name[cp - n] == 0 || vp->name[cp - n] == '='))
			return(vp);
	vp = (struct var *)malloc(sizeof(*vp));
	if (vp == 0 || (vp->name = malloc(cp-n+2)) == 0) {
		dummy.name = dummy.value = "";
		return(&dummy);
	}
	for (cp = vp->name; (*cp = *n++) && *cp != '='; cp++)
		;
	if (*cp == 0)
		*cp = '=';
	*++cp = 0;
	setarea((char *)vp, 0);
	setarea((char *)vp->name, 0);
	vp->value = null;
	vp->next = vlist;
	vp->status = GETCELL;
	vlist = vp;
	return(vp);
}

/*
 * give variable at `vp' the value `val'.
 */
static void
setval(struct var *vp, char *val)
{
	nameval(vp, val, (char *)NULL);
}

/*
 * if name is not NULL, it must be
 * a prefix of the space `val',
 * and end with `='.
 * this is all so that exporting
 * values is reasonably painless.
 */
static void
nameval(struct var *vp, char *val, char *name)
{
	char *nv;
	int fl;

	if (vp->status & RONLY) {
		sh_errf("%*.s is read-only\n", strcspn(vp->name, "="), vp->name);
		return;
	}
	fl = 0;
	if (name == NULL) {
		size_t len = strlen(vp->name);
		if (!(name = malloc(len+strlen(val)+2)))
			return;
		/* make string:  name=value */
		setarea(name, 0);
		if (!(nv = memccpy(name, vp->name, '=', len))) {
			name[len] = '=';
			nv = name + len + 1;
		}
		val = strcpy(nv, val);
		fl = GETCELL;
	}
	if (vp->status & GETCELL)
		free(vp->name);	/* form new string `name=value' */
	vp->name = name;
	vp->value = val;
	vp->status |= fl;
}

static void
export(struct var *vp)
{
	vp->status |= EXPORT;
}

static void
ronly(struct var *vp)
{
	if (letter(vp->name[0]))	/* not an internal symbol ($# etc) */
		vp->status |= RONLY;
}

static int
isassign(char *s)
{
	if (!letter((int)*s))
		return(0);
	for (; *s != '='; s++)
		if (*s == 0 || !letnum(*s))
			return(0);
	return(1);
}

static int
assign(char *s, int cf)
{
	char *cp;
	struct var *vp;

	if (!letter(*s))
		return(0);
	for (cp = s; *cp != '='; cp++)
		if (*cp == 0 || !letnum(*cp))
			return(0);
	vp = lookup(s);
	nameval(vp, ++cp, cf == COPYV? (char *)NULL: s);
	if (cf != COPYV)
		vp->status &= ~GETCELL;
	return(1);
}

static int
checkname(char *cp)
{
	if (!letter(*cp++))
		return(0);
	while (*cp)
		if (!letnum(*cp++))
			return(0);
	return(1);
}

static void
putvlist(int f)
{
	struct var *vp;

	for (vp = vlist; vp; vp = vp->next)
		if (vp->status & f && letter(*vp->name)) {
			if (vp->status & EXPORT)
				printf("export ");
			if (vp->status & RONLY)
				printf("readonly ");
			printf("%.*s\n", (int)strcspn(vp->name, "="), vp->name);
		}
}

/* -------- gmatch.c -------- */
/*
 * int gmatch(string, pattern)
 * char *string, *pattern;
 *
 * Match a pattern as in sh(1).
 */

#define	CMASK	0377
#define	QUOTE	0200
#define	QMASK	(CMASK&~QUOTE)
#define	NOT	'!'	/* might use ^ */

static int
gmatch(char *s, char *p)
{
	int sc, pc;

	if (s == NULL || p == NULL)
		return(0);
	while ((pc = *p++ & CMASK) != '\0') {
		sc = *s++ & QMASK;
		switch (pc) {
		case '[':
			if ((p = cclass(p, sc)) == NULL)
				return(0);
			break;

		case '?':
			if (sc == 0)
				return(0);
			break;

		case '*':
			s--;
			do {
				if (*p == '\0' || gmatch(s, p))
					return(1);
			} while (*s++ != '\0');
			return(0);

		default:
			if (sc != (pc&~QUOTE))
				return(0);
		}
	}
	return(*s == 0);
}

static char *
cclass(char *p, int sub)
{
	int c, d, not, found;

	if ((not = *p == NOT) != 0)
		p++;
	found = not;
	do {
		if (*p == '\0')
			return((char *)NULL);
		c = *p & CMASK;
		if (p[1] == '-' && p[2] != ']') {
			d = p[2] & CMASK;
			p++;
		} else
			d = c;
		if (c == sub || (c <= sub && sub <= d))
			found = !not;
	} while (*++p != ']');
	return(found? p+1: (char *)NULL);
}

/* -------- area.c -------- */
#define	REGSIZE		sizeof(struct region)
#define GROWBY		256
#define	SHRINKBY	64
#undef	SHRINKBY
#define FREE 32767
#define BUSY 0
#define	ALIGN (sizeof(int)-1)

/* #include "area.h" */

struct region {
	struct	region *next;
	int	area;
};

static void
setarea(char *cp, int a)
{
	struct here *p;

	if ((p = (struct here *)cp) != NULL)
		p->area = a;
}

static int
getarea(char *cp)
{
	return ((struct here*)cp)->area;
}


/* -------- csyn.c -------- */
/*
 * shell: syntax (C version)
 */

typedef union {
	char	*cp;
	char	**wp;
	int	i;
	struct	op *o;
} YYSTYPE;
#define	WORD	256
#define	LOGAND	257
#define	LOGOR	258
#define	BREAK	259
#define	IF	260
#define	THEN	261
#define	ELSE	262
#define	ELIF	263
#define	FI	264
#define	CASE	265
#define	ESAC	266
#define	FOR	267
#define	WHILE	268
#define	UNTIL	269
#define	DO	270
#define	DONE	271
#define	IN	272
#define	YYERRCODE 300

/* flags to yylex */
#define	CONTIN	01	/* skip new lines to complete command */

#define	SYNTAXERR	zzerr()
static	int	startl;
static	int	peeksym;
static	int	nlseen;
static	int	iounit = IODEFAULT;

static	YYSTYPE	yylval;

static struct op *pipeline (int cf );
static struct op *andor (void);
static struct op *c_list (void);
static int synio (int cf );
static void musthave (int c, int cf );
static struct op *simple (void);
static struct op *nested (int type, int mark );
static struct op *command (int cf );
static struct op *dogroup (int onlydone );
static struct op *thenpart (void);
static struct op *elsepart (void);
static struct op *caselist (void);
static struct op *casepart (void);
static char **pattern (void);
static char **wordlist (void);
static struct op *list (struct op *t1, struct op *t2 );
static struct op *block (int type, struct op *t1, struct op *t2, char **wp );
static struct op *newtp (void);
static struct op *namelist (struct op *t );
static char **copyw (void);
static void word (char *cp );
static struct ioword **copyio (void);
static struct ioword *io2 (int u, int f, char *cp );
static void zzerr (void);
static void yyerror (char *s );
static int yylex (int cf );
static int collect (int c, int c1 );
static int dual (int c );
static void diag (int ec );

static int
yyparse(void)
{
	startl  = 1;
	peeksym = 0;
	yynerrs = 0;
	outtree = c_list();
	musthave('\n', 0);
	return(yynerrs!=0);
}

static struct op *
pipeline(int cf)
{
	struct op *t, *p;
	int c;

	t = command(cf);
	if (t != NULL) {
		while ((c = yylex(0)) == '|') {
			if ((p = command(CONTIN)) == NULL)
				SYNTAXERR;
			if (t->type != TPAREN && t->type != TCOM) {
				/* shell statement */
				t = block(TPAREN, t, NOBLOCK, NOWORDS);
			}
			t = block(TPIPE, t, p, NOWORDS);
		}
		peeksym = c;
	}
	return(t);
}

static struct op *
andor(void)
{
	struct op *t, *p;
	int c;

	t = pipeline(0);
	if (t != NULL) {
		while ((c = yylex(0)) == LOGAND || c == LOGOR) {
			if ((p = pipeline(CONTIN)) == NULL)
				SYNTAXERR;
			t = block(c == LOGAND? TAND: TOR, t, p, NOWORDS);
		}
		peeksym = c;
	}
	return(t);
}

static struct op *
c_list()
{
	struct op *t, *p;
	int c;

	t = andor();
	if (t != NULL) {
		if((peeksym = yylex(0)) == '&')
			t = block(TASYNC, t, NOBLOCK, NOWORDS);
		while ((c = yylex(0)) == ';' || c == '&' || (multiline && c == '\n')) {
			if ((p = andor()) == NULL)
				return(t);
			if((peeksym = yylex(0)) == '&')
				p = block(TASYNC, p, NOBLOCK, NOWORDS);
			t = list(t, p);
		}
		peeksym = c;
	}
	return(t);
}


static int
synio(int cf)
{
	struct ioword *iop;
	int i;
	int c;

	if ((c = yylex(cf)) != '<' && c != '>') {
		peeksym = c;
		return(0);
	}
	i = yylval.i;
	musthave(WORD, 0);
	iop = io2(iounit, i, yylval.cp);
	iounit = IODEFAULT;
	if (i & IOHERE)
		markhere(yylval.cp, iop);
	return(1);
}

static void
musthave(int c, int cf)
{
	if ((peeksym = yylex(cf)) != c)
		SYNTAXERR;
	peeksym = 0;
}

static struct op *
simple(void)
{
	struct op *t;

	t = NULL;
	for (;;) {
		switch (peeksym = yylex(0)) {
		case '<':
		case '>':
			(void) synio(0);
			break;

		case WORD:
			if (t == NULL) {
				t = newtp();
				t->type = TCOM;
			}
			peeksym = 0;
			word(yylval.cp);
			break;

		default:
			return(t);
		}
	}
}

static struct op *
nested(int type, int mark)
{
	struct op *t;

	multiline++;
	t = c_list();
	musthave(mark, 0);
	multiline--;
	return(block(type, t, NOBLOCK, NOWORDS));
}

static struct op *
command(int cf)
{
	struct op *t;
	struct wdblock *iosave;
	int c;

	iosave = iolist;
	iolist = NULL;
	if (multiline)
		cf |= CONTIN;
	while (synio(cf))
		cf = 0;
	switch (c = yylex(cf)) {
	default:
		peeksym = c;
		if ((t = simple()) == NULL) {
			if (iolist == NULL)
				return((struct op *)NULL);
			t = newtp();
			t->type = TCOM;
		}
		break;

	case '(':
		t = nested(TPAREN, ')');
		break;

	case '{':
		t = nested(TBRACE, '}');
		break;

	case FOR:
		t = newtp();
		t->type = TFOR;
		musthave(WORD, 0);
		startl = 1;
		t->str = yylval.cp;
		multiline++;
		t->words = wordlist();
		if ((c = yylex(0)) != '\n' && c != ';')
			peeksym = c;
		t->left = dogroup(0);
		multiline--;
		break;

	case WHILE:
	case UNTIL:
		multiline++;
		t = newtp();
		t->type = c == WHILE? TWHILE: TUNTIL;
		t->left = c_list();
		t->right = dogroup(1);
		t->words = NULL;
		multiline--;
		break;

	case CASE:
		t = newtp();
		t->type = TCASE;
		musthave(WORD, 0);
		t->str = yylval.cp;
		startl++;
		multiline++;
		musthave(IN, CONTIN);
		startl++;
		t->left = caselist();
		musthave(ESAC, 0);
		multiline--;
		break;

	case IF:
		multiline++;
		t = newtp();
		t->type = TIF;
		t->left = c_list();
		t->right = thenpart();
		musthave(FI, 0);
		multiline--;
		break;
	}
	while (synio(0))
		;
	t = namelist(t);
	iolist = iosave;
	return(t);
}

static struct op *
dogroup(int onlydone)
{
	int c;
	struct op *list;

	c = yylex(CONTIN);
	if (c == DONE && onlydone)
		return((struct op *)NULL);
	if (c != DO)
		SYNTAXERR;
	list = c_list();
	musthave(DONE, 0);
	return(list);
}

static struct op *
thenpart(void)
{
	int c;
	struct op *t;

	if ((c = yylex(0)) != THEN) {
		peeksym = c;
		return((struct op *)NULL);
	}
	t = newtp();
	t->type = 0;
	t->left = c_list();
	if (t->left == NULL)
		SYNTAXERR;
	t->right = elsepart();
	return(t);
}

static struct op *
elsepart(void)
{
	int c;
	struct op *t;

	switch (c = yylex(0)) {
	case ELSE:
		if ((t = c_list()) == NULL)
			SYNTAXERR;
		return(t);

	case ELIF:
		t = newtp();
		t->type = TELIF;
		t->left = c_list();
		t->right = thenpart();
		return(t);

	default:
		peeksym = c;
		return((struct op *)NULL);
	}
}

static struct op *
caselist(void)
{
	struct op *t;

	t = NULL;
	while ((peeksym = yylex(CONTIN)) != ESAC)
		t = list(t, casepart());
	return(t);
}

static struct op *
casepart(void)
{
	struct op *t;

	t = newtp();
	t->type = TPAT;
	t->words = pattern();
	musthave(')', 0);
	t->left = c_list();
	if ((peeksym = yylex(CONTIN)) != ESAC)
		musthave(BREAK, CONTIN);
	return(t);
}

static char **
pattern(void)
{
	int c, cf;

	cf = CONTIN;
	do {
		musthave(WORD, cf);
		word(yylval.cp);
		cf = 0;
	} while ((c = yylex(0)) == '|');
	peeksym = c;
	word(NOWORD);
	return(copyw());
}

static char **
wordlist(void)
{
	int c;

	if ((c = yylex(0)) != IN) {
		peeksym = c;
		return((char **)NULL);
	}
	startl = 0;
	while ((c = yylex(0)) == WORD)
		word(yylval.cp);
	word(NOWORD);
	peeksym = c;
	return(copyw());
}

/*
 * supporting functions
 */
static struct op *
list(struct op *t1, struct op *t2)
{
	if (t1 == NULL)
		return(t2);
	if (t2 == NULL)
		return(t1);
	return(block(TLIST, t1, t2, NOWORDS));
}

static struct op *
block(int type, struct op *t1, struct op *t2, char **wp)
{
	struct op *t;

	t = newtp();
	t->type = type;
	t->left = t1;
	t->right = t2;
	t->words = wp;
	return(t);
}

static int
rlookup(char *n)
{
	struct res {
		const char	*r_name;
		int	r_val;
	} restab[] = {
		{ "for",	FOR },
		{ "case",	CASE },
		{ "esac",	ESAC },
		{ "while",	WHILE },
		{ "do",		DO },
		{ "done",	DONE },
		{ "if",		IF },
		{ "in",		IN },
		{ "then",	THEN },
		{ "else",	ELSE },
		{ "elif",	ELIF },
		{ "until",	UNTIL },
		{ "fi",		FI },

		{ ";;",		BREAK },
		{ "||",		LOGOR },
		{ "&&",		LOGAND },
		{ "{",		'{' },
		{ "}",		'}' },

		{0},
	};

	struct res *rp;

	for (rp = restab; rp->r_name; rp++)
		if (strcmp(rp->r_name, n) == 0)
			return(rp->r_val);
	return(0);
}

static struct op *
newtp(void)
{
	struct op *t;

	t = (struct op *)malloc(sizeof(*t));
	if (!t)
		fail();
	t->type = 0;
	t->words = NULL;
	t->ioact = NULL;
	t->left = NULL;
	t->right = NULL;
	t->str = NULL;
	return(t);
}

static struct op *
namelist(struct op *t)
{
	if (iolist) {
		iolist = addword((char *)NULL, iolist);
		t->ioact = copyio();
	} else
		t->ioact = NULL;
	if (t->type != TCOM) {
		if (t->type != TPAREN && t->ioact != NULL) {
			t = block(TPAREN, t, NOBLOCK, NOWORDS);
			t->ioact = t->left->ioact;
			t->left->ioact = NULL;
		}
		return(t);
	}
	word(NOWORD);
	t->words = copyw();
	return(t);
}

static char **
copyw(void)
{
	char **wd;

	wd = getwords(wdlist);
	wdlist = 0;
	return(wd);
}

static void
word(char *cp)
{
	wdlist = addword(cp, wdlist);
}

static struct ioword **
copyio(void)
{
	struct ioword **iop;

	iop = (struct ioword **) getwords(iolist);
	iolist = 0;
	return(iop);
}

static struct ioword *
io2(int u, int f, char *cp)
{
	struct ioword *iop;

	iop = (struct ioword *) malloc(sizeof(*iop));
	if (!iop)
		fail();
	iop->io_unit = u;
	iop->io_flag = f;
	iop->io_name = cp;
	iop->io_fd = -1;
	iolist = addword((char *)iop, iolist);
	return(iop);
}

static void
zzerr(void)
{
	yyerror("syntax error");
}

static void
yyerror(char *s)
{
	yynerrs++;
	if (talking && e.iop <= iostack) {
		multiline = 0;
		while (eofc() == 0 && yylex(0) != '\n')
			;
	}
	sh_errf("%s\n", s);
	fail();
}

static int
yylex(int cf)
{
	int c, c1;
	int atstart;

	if ((c = peeksym) > 0) {
		peeksym = 0;
		if (c == '\n')
			startl = 1;
		return(c);
	}
	nlseen = 0;
	e.linep = line;
	atstart = startl;
	startl = 0;
	yylval.i = 0;

loop:
	while ((c = mygetc(0)) == ' ' || c == '\t')
		;
	switch (c) {
	default:
		if (strchr("0123456789", c)) {
			unget(c1 = mygetc(0));
			if (c1 == '<' || c1 == '>') {
				iounit = c - '0';
				goto loop;
			}
			*e.linep++ = c;
			c = c1;
		}
		break;

	case '#':
		while ((c = mygetc(0)) != 0 && c != '\n')
			;
		unget(c);
		goto loop;

	case 0:
		return(c);

	case '$':
		*e.linep++ = c;
		if ((c = mygetc(0)) == '{') {
			if ((c = collect(c, '}')) != '\0')
				return(c);
			goto pack;
		}
		break;

	case '`':
	case '\'':
	case '"':
		if ((c = collect(c, c)) != '\0')
			return(c);
		goto pack;

	case '|':
	case '&':
	case ';':
		startl = 1;
		if ((c1 = dual(c)) != '\0')
			return(c1);
		return(c);
	case '^':
		startl = 1;
		return('|');
	case '>':
	case '<':
		diag(c);
		return(c);

	case '\n':
		nlseen++;
		gethere();
		startl = 1;
		if (multiline || cf & CONTIN) {
			if (talking && e.iop <= iostack)
				prs(cprompt->value);
			if (cf & CONTIN)
				goto loop;
		}
		return(c);

	case '(':
	case ')':
		startl = 1;
		return(c);
	}

	unget(c);

pack:
	while ((c = mygetc(0)) != 0 && !strchr("`$ '\"\t;&<>()|^\n", c)) {
		if (e.linep >= elinep)
			sh_errf("word too long\n");
		else
			*e.linep++ = c;
	}
	unget(c);
	if (strchr("\"'`$", c))
		goto loop;
	*e.linep++ = '\0';
	if (atstart && (c = rlookup(line))!=0) {
		startl = 1;
		return(c);
	}
	yylval.cp = strsave(line, areanum);
	return(WORD);
}

static int
collect(int c, int c1)
{
	char s[2];

	*e.linep++ = c;
	while ((c = mygetc(c1)) != c1) {
		if (c == 0) {
			unget(c);
			s[0] = c1;
			s[1] = 0;
			prs("no closing "); yyerror(s);
			return(YYERRCODE);
		}
		if (talking && c == '\n' && e.iop <= iostack)
			prs(cprompt->value);
		*e.linep++ = c;
	}
	*e.linep++ = c;
	return(0);
}

static int
dual(int c)
{
	char s[3];
	char *cp = s;

	*cp++ = c;
	*cp++ = mygetc(0);
	*cp = 0;
	if ((c = rlookup(s)) == 0)
		unget(*--cp);
	return(c);
}

static void
diag(int ec)
{
	int c;

	c = mygetc(0);
	if (c == '>' || c == '<') {
		if (c != ec)
			zzerr();
		yylval.i = ec == '>'? IOWRITE|IOCAT: IOHERE;
		c = mygetc(0);
	} else
		yylval.i = ec == '>'? IOWRITE: IOREAD;
	if (c != '&' || yylval.i == IOHERE)
		unget(c);
	else
		yylval.i |= IODUP;
}


/* -------- exec.c -------- */

/*
 * execute tree
 */

static int forkexec (struct op *t, int *pin, int *pout, int act, char **wp, int *pforked );
static int parent (void);
static int iosetup (struct ioword *iop, int pipein, int pipeout );
static void echo (char **wp );
static struct op **find1case (struct op *t, char *w );
static struct op *findcase (struct op *t, char *w );
static void brkset (struct brkcon *bc );
static int dolabel (struct op *t );
static int dochdir (struct op *t );
static int doshift (struct op *t );
static int dologin (struct op *t );
static int doumask (struct op *t );
static int doexec (struct op *t );
static int dodot (struct op *t );
static int dowait (struct op *t );
static int doread (struct op *t );
static int doeval (struct op *t );
static int dotrap (struct op *t );
static int getsig (char *s );
static void setsig (int n, void (*f)(int));
static int getn (char *as );
static int dobreak (struct op *t );
static int docontinue (struct op *t );
static int brkcontin (char *cp, int val );
static int doexit (struct op *t );
static int doexport (struct op *t );
static int doreadonly (struct op *t );
static void rdexp (char **wp, void (*f)(struct var *), int key);
static void badid (char *s );
static int doset (struct op *t );
static void varput (char *s);
static int dotimes (struct op *t);

static int
execute(struct op *t, int *pin, int *pout, int act)
{
	struct op *t1;
	int i, pv[2], rv, child, a;
	char *cp, **wp, **wp2;
	struct var *vp;
	struct brkcon bc;

	if (t == NULL)
		return(0);
	rv = 0;
	a = areanum++;
	wp = (wp2 = t->words) != NULL
	     ? eval(wp2, t->type == TCOM ? DOALL : DOALL & ~DOKEY)
	     : NULL;

	switch (t->type) {
	case TPAREN:
	case TCOM:
		rv = forkexec(t, pin, pout, act, wp, &child);
		if (child) {
			exstat = rv;
			leave();
		}
		break;

	case TPIPE:
		if ((rv = openpipe(pv)) < 0)
			break;
		pv[0] = remap(pv[0]);
		pv[1] = remap(pv[1]);
		(void) execute(t->left, pin, pv, 0);
		rv = execute(t->right, pv, pout, 0);
		break;

	case TLIST:
		(void) execute(t->left, pin, pout, 0);
		rv = execute(t->right, pin, pout, 0);
		break;

	case TASYNC:
		i = parent();
		if (i != 0) {
			if (i != -1) {
				setval(lookup("!"), putn(i));
				if (pin != NULL)
					closepipe(pin);
				if (talking) {
					fprintf(stderr, "%d\n", i);
				}
			} else
				rv = -1;
			setstatus(rv);
		} else {
			signal(SIGINT, SIG_IGN);
			signal(SIGQUIT, SIG_IGN);
			if (talking)
				signal(SIGTERM, SIG_DFL);
			talking = 0;
			if (pin == NULL) {
				close(0);
				if (open(_PATH_DEVNULL, O_RDONLY) == -1)
					warn(_PATH_DEVNULL);
			}
			exit(execute(t->left, pin, pout, FEXEC));
		}
		break;

	case TOR:
	case TAND:
		rv = execute(t->left, pin, pout, 0);
		if ((t1 = t->right)!=NULL && (rv == 0) == (t->type == TAND))
			rv = execute(t1, pin, pout, 0);
		break;

	case TFOR:
		if (wp == NULL) {
			wp = dolv+1;
			if ((i = dolc) < 0)
				i = 0;
		} else {
			i = -1;
			while (*wp++ != NULL)
				;
		}
		vp = lookup(t->str);
		while (setjmp(bc.brkpt))
			if (isbreak)
				goto broken;
		brkset(&bc);
		for (t1 = t->left; i-- && *wp != NULL;) {
			setval(vp, *wp++);
			rv = execute(t1, pin, pout, 0);
		}
		brklist = brklist->nextlev;
		break;

	case TWHILE:
	case TUNTIL:
		while (setjmp(bc.brkpt))
			if (isbreak)
				goto broken;
		brkset(&bc);
		t1 = t->left;
		while ((execute(t1, pin, pout, 0) == 0) == (t->type == TWHILE))
			rv = execute(t->right, pin, pout, 0);
		brklist = brklist->nextlev;
		break;

	case TIF:
	case TELIF:
		if (t->right != NULL) {
		rv = !execute(t->left, pin, pout, 0) ?
			execute(t->right->left, pin, pout, 0):
			execute(t->right->right, pin, pout, 0);
		}
		break;

	case TCASE:
		if ((cp = evalstr(t->str, DOSUB|DOTRIM)) == 0)
			cp = "";
		if ((t1 = findcase(t->left, cp)) != NULL)
			rv = execute(t1, pin, pout, 0);
		break;

	case TBRACE:
/*
		if (iopp = t->ioact)
			while (*iopp)
				if (iosetup(*iopp++, pin!=NULL, pout!=NULL)) {
					rv = -1;
					break;
				}
*/
		if (rv >= 0 && (t1 = t->left))
			rv = execute(t1, pin, pout, 0);
		break;
	}

broken:
	t->words = wp2;
	isbreak = 0;
	freehere(areanum);
	areanum = a;
	if (talking && intr) {
		closeall();
		fail();
	}
	if ((i = trapset) != 0) {
		trapset = 0;
		runtrap(i);
	}
	return(rv);
}

static int
forkexec(struct op *t, int *pin, int *pout, int act, char **wp, int *pforked)
{
	int i, rv, (*shcom)(struct op *);
	int f;
	char *cp;
	struct ioword **iopp;
	int resetsig;
	char **owp;

	owp = wp;
	resetsig = 0;
	*pforked = 0;
	shcom = NULL;
	rv = -1;	/* system-detected error */
	if (t->type == TCOM) {
		while (*wp++ != NULL)
			;
		cp = *wp;

		/* strip all initial assignments */
		/* not correct wrt PATH=yyy command  etc */
		if (flag['x'])
			echo (cp ? wp: owp);
		if (cp == NULL && t->ioact == NULL) {
			while ((cp = *owp++) != NULL && assign(cp, COPYV))
				;
			return(setstatus(0));
		}
		else if (cp != NULL)
			shcom = inbuilt(cp);
	}
	t->words = wp;
	f = act;
	if (shcom == NULL && (f & FEXEC) == 0) {
		i = parent();
		if (i != 0) {
			if (i == -1)
				return(rv);
			if (pin != NULL)
				closepipe(pin);
			return(pout==NULL? setstatus(waitfor(i,0)): 0);
		}
		if (talking) {
			signal(SIGINT, SIG_IGN);
			signal(SIGQUIT, SIG_IGN);
			resetsig = 1;
		}
		talking = 0;
		intr = 0;
		(*pforked)++;
		brklist = 0;
		execflg = 0;
	}
	if (owp != NULL)
		while ((cp = *owp++) != NULL && assign(cp, COPYV))
			if (shcom == NULL)
				export(lookup(cp));
#ifdef COMPIPE
	if ((pin != NULL || pout != NULL) && shcom != NULL && shcom != doexec) {
		sh_errf("piping to/from shell builtins not yet done\n");
		return(-1);
	}
#endif
	if (pin != NULL) {
		dup2(pin[0], 0);
		closepipe(pin);
	}
	if (pout != NULL) {
		dup2(pout[1], 1);
		closepipe(pout);
	}
	if ((iopp = t->ioact) != NULL) {
		if (shcom != NULL && shcom != doexec) {
			sh_errf("%s: cannot redirect shell command\n", cp);
			return(-1);
		}
		while (*iopp)
			if (iosetup(*iopp++, pin!=NULL, pout!=NULL))
				return(rv);
	}
	if (shcom)
		return(setstatus((*shcom)(t)));
	/* should use FIOCEXCL */
	for (i = FDBASE; i < NOFILE_; i++)
		close(i);
	if (resetsig) {
		signal(SIGINT, SIG_DFL);
		signal(SIGQUIT, SIG_DFL);
	}
	if (t->type == TPAREN)
		exit(execute(t->left, NOPIPE, NOPIPE, FEXEC));
	if (wp[0] == NULL)
		exit(0);
	cp = rexecve(wp[0], wp, makenv());
	sh_warnf("%s: %s\n", wp[0], cp);
	if (!execflg)
		trap[0] = NULL;
	leave();
	/* NOTREACHED */
}

/*
 * common actions when creating a new child
 */
static int
parent(void)
{
	int i;

	i = fork();
	if (i != 0) {
		if (i == -1)
			sh_warnf("try again\n");
	}
	return(i);
}

/*
 * 0< 1> are ignored as required
 * within pipelines.
 */
static int
iosetup(struct ioword *iop, int pipein, int pipeout)
{
	int u;
	char *cp, *msg;

	if (iop->io_unit == IODEFAULT)	/* take default */
		iop->io_unit = !(iop->io_flag&(IOREAD|IOHERE));
	if (pipein && iop->io_unit == 0)
		return(0);
	if (pipeout && iop->io_unit == 1)
		return(0);
	msg = (iop->io_flag&(IOREAD|IOHERE)) ? "open" : "create";
	if ((iop->io_flag & IOHERE) == 0) {
		cp = iop->io_name;
		if ((cp = evalstr(cp, DOSUB|DOTRIM)) == NULL)
			return(1);
	}
	if (iop->io_flag & IODUP) {
		if (cp[1] || (!digit(*cp) && *cp != '-')) {
			sh_errf("%s: illegal >& argument", cp);
			return(1);
		}
		if (*cp == '-')
			iop->io_flag = IOCLOSE;
		iop->io_flag &= ~(IOREAD|IOWRITE);
	}
	switch (iop->io_flag) {
	case IOREAD:
		u = open(cp, 0);
		break;

	case IOHERE:
	case IOHERE|IOXHERE:
		u = herein(iop->io_name, iop->io_flag&IOXHERE);
		cp = "here file ";
		break;

	case IOWRITE|IOCAT:
		if ((u = open(cp, 1)) >= 0) {
			lseek(u, (long)0, 2);
			break;
		}
	case IOWRITE:
		u = creat(cp, 0666);
		break;

	case IODUP:
		u = dup2(*cp-'0', iop->io_unit);
		break;

	case IOCLOSE:
		close(iop->io_unit);
		return(0);
	}
	if (u < 0) {
		int e=errno;
		prs(cp);
                if (iop->io_flag&IOHERE) prs(iop->io_name);
		sh_warnf(": cannot %s (%s)\n", msg, strerror(e));
		return(1);
	} else {
		if (u != iop->io_unit) {
			dup2(u, iop->io_unit);
			close(u);
		}
	}
	return(0);
}

static void
echo(char **wp)
{
	int i;

	prs("+");
	for (i=0; wp[i]; i++) {
		if (i)
			prs(" ");
		prs(wp[i]);
	}
	prs("\n");
}

static struct op **
find1case(struct op *t, char *w)
{
	struct op *t1;
	struct op **tp;
	char **wp, *cp;

	if (t == NULL)
		return((struct op **)NULL);
	if (t->type == TLIST) {
		if ((tp = find1case(t->left, w)) != NULL)
			return(tp);
		t1 = t->right;	/* TPAT */
	} else
		t1 = t;
	for (wp = t1->words; *wp;)
		if ((cp = evalstr(*wp++, DOSUB)) && gmatch(w, cp))
			return(&t1->left);
	return((struct op **)NULL);
}

static struct op *
findcase(struct op *t, char *w)
{
	struct op **tp;

	return((tp = find1case(t, w)) != NULL? *tp: (struct op *)NULL);
}

/*
 * Enter a new loop level (marked for break/continue).
 */
static void
brkset(struct brkcon *bc)
{
	bc->nextlev = brklist;
	brklist = bc;
}

/*
 * Wait for the last process created.
 * Print a message for each process found
 * that was killed by a signal.
 * Ignore interrupt signals while waiting
 * unless `canintr' is true.
 */
static int
waitfor(int lastpid, int canintr)
{
	int pid, rv;
	int s;
	bool oheedint = heedint;

	heedint = 0;
	rv = 0;
	do {
		pid = wait(&s);
		if (pid == -1) {
			if (errno != EINTR || canintr)
				break;
		} else {
			if ((rv = WTERMSIG(s)) != 0) {
				if (pid != lastpid)
					fprintf(stderr, "%d: ", pid);
				char *signame = strsignal(rv);
				if (signame)
					fprintf(stderr, "%s", signame);
				else
					fprintf(stderr, "Signal %d ", rv);
				if (WCOREDUMP(s))
					fprintf(stderr, " - core dumped");
				fprintf(stderr, "\n");
				rv = -1;
			} else
				rv = WEXITSTATUS(s);
		}
	} while (pid != lastpid);
	heedint = oheedint;
	if (intr) {
		if (talking) {
			if (canintr)
				intr = 0;
		} else {
			if (exstat == 0) exstat = rv;
			onintr(0);
		}
	}
	return(rv);
}

static int
setstatus(int s)
{
	exstat = s;
	setval(lookup("?"), putn(s));
	return(s);
}

/*
 * PATH-searching interface to execve.
 * If getenv("PATH") were kept up-to-date,
 * execvp might be used.
 */
char *
rexecve(char *c, char **v, char **envp)
{
	int i;
	char *sp, *tp;
	int eacces = 0, asis = 0;

	sp = strchr(c, '/') ? "" : path->value;
	asis = *sp == '\0';
	while (asis || *sp != '\0') {
		asis = 0;
		tp = e.linep;
		for (; *sp != '\0'; tp++)
			if ((*tp = *sp++) == ':') {
				asis = *sp == '\0';
				break;
			}
		if (tp != e.linep)
			*tp++ = '/';
		for (i = 0; (*tp++ = c[i++]) != '\0';)
			;
		execve(e.linep, v, envp);
		switch (errno) {
		case ENOEXEC:
			*v = e.linep;
			tp = *--v;
			*v = e.linep;
			execve("/bin/sh", v, envp);
			*v = tp;
			return("no Shell");

		case ENOMEM:
			return("program too big");

		case E2BIG:
			return("argument list too long");

		case EACCES:
			eacces++;
			break;
		}
	}
	return(errno==ENOENT ? "not found" : "cannot execute");
}

/*
 * Run the command produced by generator `f'
 * applied to stream `arg'.
 */
static int
run(struct ioarg *argp, int (*f)(struct ioarg *, struct io *))
{
	struct op *otree;
	struct wdblock *swdlist;
	struct wdblock *siolist;
	jmp_buf ev, rt;
	xint *ofail;
	int rv;

	areanum++;
	swdlist = wdlist;
	siolist = iolist;
	otree = outtree;
	ofail = failpt;
	rv = -1;
	if (newenv(setjmp(errpt = ev)) == 0) {
		wdlist = 0;
		iolist = 0;
		pushio(argp, f);
		e.iobase = e.iop;
		yynerrs = 0;
		if (setjmp(failpt = rt) == 0 && yyparse() == 0)
			rv = execute(outtree, NOPIPE, NOPIPE, 0);
		quitenv();
	}
	wdlist = swdlist;
	iolist = siolist;
	failpt = ofail;
	outtree = otree;
	areanum--;
	return(rv);
}

/* -------- do.c -------- */

/*
 * built-in commands: doX
 */

static int
dolabel(struct op *t)
{
	return(0);
}

static int
dochdir(struct op *t)
{
	char *cp;

	if ((cp = t->words[1]) == NULL && (cp = homedir->value) == NULL)
		sh_errf("cd: no home directory\n");
	else if(chdir(cp) < 0)
		sh_errf("%s: bad directory\n", cp);
	else
		return(0);
	return(1);
}

static int
doshift(struct op *t)
{
	int n;

	n = t->words[1]? getn(t->words[1]): 1;
	if (dolc < n) {
		sh_errf("nothing to shift\n");
		return(1);
	}
	dolv[n] = dolv[0];
	dolv += n;
	dolc -= n;
	setval(lookup("#"), putn(dolc));
	return(0);
}

/*
 * execute login and newgrp directly
 */
static int
dologin(struct op *t)
{
	char *cp;

	if (talking) {
		signal(SIGINT, SIG_DFL);
		signal(SIGQUIT, SIG_DFL);
	}
	cp = rexecve(t->words[0], t->words, makenv());
	sh_errf("%s: %s\n", t->words[0], cp);
	return(1);
}

static int
doumask(struct op *t)
{
	int i, n;
	char *cp;

	if ((cp = t->words[1]) == NULL) {
		i = umask(0);
		umask(i);
		fprintf(stderr, "%04o\n", i);
	} else {
		for (n=0; *cp>='0' && *cp<='9'; cp++)
			n = n*8 + (*cp-'0');
		umask(n);
	}
	return(0);
}

static int
doexec(struct op *t)
{
	int i;
	jmp_buf ex;
	xint *ofail;

	t->ioact = NULL;
	for (i = 0; (t->words[i]=t->words[i+1]) != NULL; i++)
		;
	if (i == 0)
		return(1);
	execflg = 1;
	ofail = failpt;
	if (setjmp(failpt = ex) == 0)
		execute(t, NOPIPE, NOPIPE, FEXEC);
	failpt = ofail;
	execflg = 0;
	return(1);
}

static int
dodot(struct op *t)
{
	int i;
	char *sp, *tp;
	char *cp;

	if ((cp = t->words[1]) == NULL)
		return(0);
	sp = strchr(cp, '/') ? ":" : path->value;
	while (*sp) {
		tp = e.linep;
		while (*sp && (*tp = *sp++) != ':')
			tp++;
		if (tp != e.linep)
			*tp++ = '/';
		for (i = 0; (*tp++ = cp[i++]) != '\0';)
			;
		if ((i = open(e.linep, 0)) >= 0) {
			exstat = 0;
			next(remap(i));
			return(exstat);
		}
	}
	sh_errf("%s: not found\n", cp);
	return(-1);
}

static int
dowait(struct op *t)
{
	int i;
	char *cp;

	if ((cp = t->words[1]) != NULL) {
		i = getn(cp);
		if (i == 0)
			return(0);
	} else
		i = -1;
	setstatus(waitfor(i, 1));
	return(0);
}

static int
doread(struct op *t)
{
	char *cp, **wp;
	int nb;
	int  nl = 0;

	if (t->words[1] == NULL) {
		sh_errf("Usage: read name ...\n");
		return(1);
	}
	for (wp = t->words+1; *wp; wp++) {
		for (cp = e.linep; !nl && cp < elinep-1; cp++)
			if ((nb = read(0, cp, sizeof(*cp))) != sizeof(*cp) ||
			    (nl = (*cp == '\n')) ||
			    (wp[1] && strchr(ifs->value, *cp)))
				break;
		*cp = 0;
		if (nb <= 0)
			break;
		setval(lookup(*wp), e.linep);
	}
	return(nb <= 0);
}

static int
doeval(struct op *t)
{
	return(RUN(awordlist, t->words+1, wdchar));
}

static int
dotrap(struct op *t)
{
	int  n, i;
	int  resetsig;

	if (t->words[1] == NULL) {
		for (i=0; i<=_NSIG; i++)
			if (trap[i]) {
				fprintf(stderr, "%d: %s\n", i, trap[i]);
			}
		return(0);
	}
	resetsig = digit(*t->words[1]);
	for (i = resetsig ? 1 : 2; t->words[i] != NULL; ++i) {
		n = getsig(t->words[i]);
		free(trap[n]);
		trap[n] = 0;
		if (!resetsig) {
			if (*t->words[1] != '\0') {
				trap[n] = strsave(t->words[1], 0);
				setsig(n, sig);
			} else
				setsig(n, SIG_IGN);
		} else {
			if (talking)
				if (n == SIGINT)
					setsig(n, onintr);
				else
					setsig(n, n == SIGQUIT ? SIG_IGN 
							       : SIG_DFL);
			else
				setsig(n, SIG_DFL);
		}
	}
	return(0);
}

static int
getsig(char *s)
{
	int n;

	if ((n = getn(s)) < 0 || n > _NSIG) {
		sh_errf("trap: bad signal number\n");
		n = 0;
	}
	return(n);
}

static void
setsig(int n, void(*f)(int))
{
	if (n == 0)
		return;
	if (signal(n, SIG_IGN) != SIG_IGN || ourtrap[n]) {
		ourtrap[n] = 1;
		signal(n, f);
	}
}

static int
getn(char *as)
{
	char *s;
	int n, m;

	s = as;
	m = 1;
	if (*s == '-') {
		m = -1;
		s++;
	}
	for (n = 0; digit(*s); s++)
		n = (n*10) + (*s-'0');
	if (*s)
		sh_errf("%s: bad number\n", as);
	return(n*m);
}

static int
dobreak(struct op *t)
{
	return(brkcontin(t->words[1], 1));
}

static int
docontinue(struct op *t)
{
	return(brkcontin(t->words[1], 0));
}

static int
brkcontin(char *cp, int val)
{
	struct brkcon *bc;
	int nl;

	nl = cp == NULL? 1: getn(cp);
	if (nl <= 0)
		nl = 999;
	do {
		if ((bc = brklist) == NULL)
			break;
		brklist = bc->nextlev;
	} while (--nl);
	if (nl) {
		sh_errf("bad break/continue level\n");
		return(1);
	}
	isbreak = val;
	longjmp(bc->brkpt, 1);
	/* NOTREACHED */
}

static int
doexit(struct op *t)
{
	char *cp;

	execflg = 0;
	if ((cp = t->words[1]) != NULL)
		setstatus(getn(cp));
	leave();
	/* NOTREACHED */
}

static int
doexport(struct op *t)
{
	rdexp(t->words+1, export, EXPORT);
	return(0);
}

static int
doreadonly(struct op *t)
{
	rdexp(t->words+1, ronly, RONLY);
	return(0);
}

static void
rdexp(char **wp, void (*f)(struct var *), int key)
{
	if (*wp != NULL) {
		for (; *wp != NULL; wp++)
			if (checkname(*wp))
				(*f)(lookup(*wp));
			else
				badid(*wp);
	} else
		putvlist(key);
}

static void
badid(char *s)
{
	prs(s);
	sh_errf(": bad identifier\n");
}

static int
doset(struct op *t)
{
	struct var *vp;
	char *cp;
	int n;

	if ((cp = t->words[1]) == NULL) {
		for (vp = vlist; vp; vp = vp->next)
			varput(vp->name);
		return(0);
	}
	if (*cp == '-') {
		/* bad: t->words++; */
		for(n = 0; (t->words[n]=t->words[n+1]) != NULL; n++)
			;
		if (*++cp == 0)
			flag['x'] = flag['v'] = 0;
		else
			for (; *cp; cp++)
				switch (*cp) {
				case 'e':
					if (!talking)
						flag['e']++;
					break;

				default:
					if (*cp>='a' && *cp<='z')
						flag[(int)*cp]++;
					break;
				}
		setdash();
	}
	if (t->words[1]) {
		t->words[0] = dolv[0];
		for (n=1; t->words[n]; n++)
			setarea((char *)t->words[n], 0);
		dolc = n-1;
		dolv = t->words;
		setval(lookup("#"), putn(dolc));
		setarea((char *)(dolv-1), 0);
	}
	return(0);
}

static void
varput(char *s)
{
	if (letnum(*s))
		puts(s);
}


#define	SECS	60L
#define	MINS	3600L

static int
dotimes(struct op *t)
{
	struct tms tbuf;

	times(&tbuf);

	fprintf(stderr, "%dm%ds %dm%ds\n", (int)(tbuf.tms_cutime / MINS), (int)((tbuf.tms_cutime % MINS) / SECS), (int)(tbuf.tms_cstime / MINS), (int)((tbuf.tms_cstime % MINS) / SECS));
	return(0);
}

struct	builtin {
	char	*command;
	int	(*fn)(struct op *);
};
static struct	builtin	builtin[] = {
	{ ":",	dolabel },
	{ "cd",		dochdir },
	{ "shift",	doshift },
	{ "exec",	doexec },
	{ "wait",	dowait },
	{ "read",	doread },
	{ "eval",	doeval },
	{ "trap",	dotrap },
	{ "break",	dobreak },
	{ "continue",	docontinue },
	{ "exit",	doexit },
	{ "export",	doexport },
	{ "readonly",	doreadonly },
	{ "set",	doset },
	{ ".",	dodot },
	{ "umask",	doumask },
	{ "login",	dologin },
	{ "newgrp",	dologin },
	{ "times",	dotimes },
	{0},
};

int (*inbuilt(char *s))(struct op *)
{
	struct builtin *bp;

	for (bp = builtin; bp->command != NULL; bp++)
		if (strcmp(bp->command, s) == 0)
			return(bp->fn);
	return((int(*)(struct op *))NULL);
}


/* -------- eval.c -------- */
/* #include "word.h" */

/*
 * ${}
 * `command`
 * blank interpretation
 * quoting
 * glob
 */

static int expand (char *cp, struct wdblock **wbp, int f );
static char *blank (int f );
static int dollar (int quoted );
static int grave (int quoted );
static void globname (char *we, char *pp );
static char *generate (char *start1, char *end1, char *middle, char *end );
static int anyspcl (struct wdblock *wb );
static void glob0 (char **a0, unsigned int a1);
static void glob1 (char **base, char **lim );
static void glob2 (char **i, char **j );
static void glob3 (char **i, char **j, char **k );

char **
eval(char **ap, int f)
{
	struct wdblock *wb;
	char **wp;
	char **wf;
	jmp_buf ev;

	wp = NULL;
	wb = NULL;
	wf = NULL;
	if (newenv(setjmp(errpt = ev)) == 0) {
		while (*ap && isassign(*ap))
			expand(*ap++, &wb, f & ~DOGLOB);
		if (flag['k']) {
			for (wf = ap; *wf; wf++) {
				if (isassign(*wf))
					expand(*wf, &wb, f & ~DOGLOB);
			}
		}
		for (wb = addword((char *)0, wb); *ap; ap++) {
			if (!flag['k'] || !isassign(*ap))
				expand(*ap, &wb, f & ~DOKEY);
		}
		wb = addword((char *)0, wb);
		wp = getwords(wb);
		quitenv();
	} else
		gflg = 1;
	return(gflg? (char **)NULL: wp);
}

/*
 * Make the exported environment from the exported
 * names in the dictionary. Keyword assignments
 * will already have been done.
 */
char **
makenv(void)

{
	struct wdblock *wb;
	struct var *vp;

	wb = NULL;
	for (vp = vlist; vp; vp = vp->next)
		if (vp->status & EXPORT)
			wb = addword(strdup(vp->name), wb);
	wb = addword((char *)0, wb);
	return(getwords(wb));
}

char *
evalstr(char *cp, int f)
{
	struct wdblock *wb;

	wb = NULL;
	if (expand(cp, &wb, f)) {
		if (wb == NULL || wb->w_nword == 0 || (cp = wb->w_words[0]) == NULL)
			cp = "";
		free(wb);
	} else
		cp = NULL;
	return(cp);
}

static int
expand(char *cp, struct wdblock **wbp, int f)
{
	jmp_buf ev;

	gflg = 0;
	if (cp == NULL)
		return(0);
	if (!strpbrk(cp, "$`'\"") &&
	    !strpbrk(cp, ifs->value) &&
	    ((f&DOGLOB)==0 || !strpbrk(cp, "[*?"))) {
		cp = strsave(cp, areanum);
		if (f & DOTRIM)
			unquote(cp);
		*wbp = addword(cp, *wbp);
		return(1);
	}
	if (newenv(setjmp(errpt = ev)) == 0) {
		PUSHIO(aword, cp, strchar);
		e.iobase = e.iop;
		while ((cp = blank(f)) && gflg == 0) {
			e.linep = cp;
			cp = strsave(cp, areanum);
			if ((f&DOGLOB) == 0) {
				if (f & DOTRIM)
					unquote(cp);
				*wbp = addword(cp, *wbp);
			} else
				*wbp = glob(cp, *wbp);
		}
		quitenv();
	} else
		gflg = 1;
	return(gflg == 0);
}

/*
 * Blank interpretation and quoting
 */
static char *
blank(int f)
{
	int c, c1;
	char *sp;
	int scanequals, foundequals;

	sp = e.linep;
	scanequals = f & DOKEY;
	foundequals = 0;

loop:
	switch (c = subgetc('"', foundequals)) {
	case 0:
		if (sp == e.linep)
			return(0);
		*e.linep++ = 0;
		return(sp);

	default:
		if (f & DOBLANK && strchr(ifs->value, c))
			goto loop;
		break;

	case '"':
	case '\'':
		scanequals = 0;
		if (INSUB())
			break;
		for (c1 = c; (c = subgetc(c1, 1)) != c1;) {
			if (c == 0)
				break;
			if (c == '\'' || !strchr("$`\"", c))
				c |= QUOTE;
			*e.linep++ = c;
		}
		c = 0;
	}
	unget(c);
	if (!letter(c))
		scanequals = 0;
	for (;;) {
		c = subgetc('"', foundequals);
		if (c == 0 ||
		    (f & DOBLANK && strchr(ifs->value, c)) ||
		    (!INSUB() && strchr("\"'", c))) {
		        scanequals = 0;
			unget(c);
			if (strchr("\"'", c))
				goto loop;
			break;
		}
		if (scanequals) {
			if (c == '=') {
				foundequals = 1;
				scanequals  = 0;
			}
			else if (!letnum(c))
				scanequals = 0;
		}
		*e.linep++ = c;
	}
	*e.linep++ = 0;
	return(sp);
}

/*
 * Get characters, substituting for ` and $
 */
static int
subgetc(int ec, int quoted)
{
	char c;

again:
	c = mygetc(ec);
	if (!INSUB() && ec != '\'') {
		if (c == '`') {
			if (grave(quoted) == 0)
				return(0);
			e.iop->task = XGRAVE;
			goto again;
		}
		if (c == '$' && (c = dollar(quoted)) == 0) {
			e.iop->task = XDOLL;
			goto again;
		}
	}
	return(c);
}

/*
 * Prepare to generate the string returned by ${} substitution.
 */
static int
dollar(int quoted)
{
	int otask;
	struct io *oiop;
	char *dolp;
	char *s, c, *cp;
	struct var *vp;

	c = readc();
	s = e.linep;
	if (c != '{') {
		*e.linep++ = c;
		if (letter(c)) {
			while ((c = readc())!=0 && letnum(c))
				if (e.linep < elinep)
					*e.linep++ = c;
			unget(c);
		}
		c = 0;
	} else {
		oiop = e.iop;
		otask = e.iop->task;
		e.iop->task = XOTHER;
		while ((c = subgetc('"', 0))!=0 && c!='}' && c!='\n')
			if (e.linep < elinep)
				*e.linep++ = c;
		if (oiop == e.iop)
			e.iop->task = otask;
		if (c != '}') {
			sh_errf("unclosed ${\n");
			gflg++;
			return(c);
		}
	}
	if (e.linep >= elinep) {
		sh_errf("string in ${} too long\n");
		gflg++;
		e.linep -= 10;
	}
	*e.linep = 0;
	if (*s)
		for (cp = s+1; *cp; cp++)
			if (strchr("=-+?", *cp)) {
				c = *cp;
				*cp++ = 0;
				break;
			}
	if (s[1] == 0 && (*s == '*' || *s == '@')) {
		if (dolc > 1) {
			/* currently this does not distinguish $* and $@ */
			/* should check dollar */
			e.linep = s;
			PUSHIO(awordlist, dolv+1, dolchar);
			return(0);
		} else {	/* trap the nasty ${=} */
			s[0] = '1';
			s[1] = 0;
		}
	}
	e.linep = s;
	vp = lookup(s);
	if ((dolp = vp->value) == null) {
		switch (c) {
		case '=':
			if (digit(*s)) {
				sh_errf("cannot use ${...=...} with $n\n");
				gflg++;
				break;
			}
			cp = evalstr(strsave(cp, areanum),DOSUB);
			setval(vp, cp);
			dolp = vp->value;
			break;

		case '-':
			dolp = evalstr(strsave(cp, areanum),DOSUB);
			break;

		case '?':
			if (*cp == 0)
				sh_errf("missing value for %s\n", s);
			else {
				const char *val = evalstr(strsave(cp, areanum),DOSUB);
				sh_errf("%s\n", *val ? val : "parameter null or not set");
			}
			gflg++;
			break;
		}
	} else if (c == '+') {
		dolp = evalstr(strsave(cp, areanum),DOSUB);
	}
	if (flag['u'] && dolp == null) {
		sh_errf("unset variable: %s\n", s);
		gflg++;
	}
	PUSHIO(aword, dolp, quoted ? qstrchar : strchar);
	return(0);
}

/*
 * Run the command in `...` and read its output.
 */
static int
grave(int quoted)
{
        int otask;
        struct io *oiop;
	char *cp,*s;
	int i,c;
	int pf[2];

	c = readc();
        s = e.linep;
        *e.linep++ = c;
	oiop = e.iop;
	otask = e.iop->task;
	e.iop->task = XOTHER;
	while ((c = subgetc('\'', 0))!=0 && c!='`')
		if (e.linep < elinep)
			*e.linep++ = c;
	if (oiop == e.iop)
		e.iop->task = otask;
	if (c != '`') {
		sh_errf("no closing `\n");
		return(0);
	}
	if (openpipe(pf) < 0)
		return(0);
	if ((i = fork()) == -1) {
		closepipe(pf);
		sh_errf("try again\n");
		return(0);
	}
	if (i != 0) {
		e.linep = s;
		close(pf[1]);
		PUSHIO(afile, remap(pf[0]), quoted? qgravechar: gravechar);
		return(1);
	}
	*e.linep = 0;
	/* allow trapped signals */
	for (i=0; i<=_NSIG; i++)
		if (ourtrap[i] && signal(i, SIG_IGN) != SIG_IGN)
			signal(i, SIG_DFL);
	dup2(pf[1], 1);
	closepipe(pf);
	flag['e'] = 0;
	flag['v'] = 0;
	flag['n'] = 0;
	cp = strsave(e.linep = s, 0);
	areanum = 1;
	inithere();
	e.oenv = NULL;
	e.iop = (e.iobase = iostack) - 1;
	unquote(cp);
	talking = 0;
	PUSHIO(aword, cp, nlchar);
	onecommand();
	exit(1);
}

char *
unquote(char *as)
{
	char *s;

	if ((s = as) != NULL)
		while (*s)
			*s++ &= ~QUOTE;
	return(as);
}

/* -------- glob.c -------- */

/*
 * glob
 */

#define	scopy(x) strsave((x), areanum)
#define	BLKSIZ	512
#define	NDENT	((BLKSIZ+sizeof(struct dirent)-1)/sizeof(struct dirent))

static	struct wdblock	*cl, *nl;
static	char	spcl[] = "[?*";

struct wdblock *
glob(char *cp, struct wdblock *wb)
{
	int i;
	char *pp;

	if (cp == 0)
		return(wb);
	i = 0;
	for (pp = cp; *pp; pp++)
		if (strchr(spcl, *pp))
			i++;
		else if (!strchr(spcl, *pp & ~QUOTE))
			*pp &= ~QUOTE;
	if (i != 0) {
		for (cl = addword(scopy(cp), (struct wdblock *)0); anyspcl(cl); cl = nl) {
			nl = newword(cl->w_nword*2);
			for(i=0; i<cl->w_nword; i++) { /* for each argument */
				for (pp = cl->w_words[i]; *pp; pp++)
					if (strchr(spcl, *pp)) {
						globname(cl->w_words[i], pp);
						break;
					}
				if (*pp == '\0')
					nl = addword(scopy(cl->w_words[i]), nl);
			}
			for(i=0; i<cl->w_nword; i++)
				free(cl->w_words[i]);
			free(cl);
		}
		for(i=0; i<cl->w_nword; i++)
			unquote(cl->w_words[i]);
		glob0(cl->w_words, cl->w_nword);
		if (cl->w_nword) {
			for (i=0; i<cl->w_nword; i++)
				wb = addword(cl->w_words[i], wb);
			free(cl);
			return(wb);
		}
	}
	wb = addword(unquote(cp), wb);
	return(wb);
}

static void
globname(char *we, char *pp)
{
	char *np, *cp;
	char *name, *gp, *dp;
	int dn, j, n, k;
	struct dirent ent[NDENT];
	char dname[NAME_MAX+1];
	struct stat dbuf;

	for (np = we; np != pp; pp--)
		if (pp[-1] == '/')
			break;
	for (dp = cp = space((int)(pp-np)+3); np < pp;)
		*cp++ = *np++;
	*cp++ = '.';
	*cp = '\0';
	for (gp = cp = space(strlen(pp)+1); *np && *np != '/';)
		*cp++ = *np++;
	*cp = '\0';
	dn = open(dp, O_RDONLY);
	if (dn < 0) {
		free(dp);
		free(gp);
		return;
	}
	dname[NAME_MAX] = '\0';
	while ((n = read(dn, (char *)ent, sizeof(ent))) >= (int)sizeof(*ent)) {
		n /= sizeof(*ent);
		for (j=0; j<n; j++) {
			if (ent[j].d_ino == 0)
				continue;
			strncpy(dname, ent[j].d_name, NAME_MAX);
			if (dname[0] == '.')
				if (*gp != '.')
					continue;
			for(k=0; k<NAME_MAX; k++)
				if (strchr(spcl, dname[k]))
					dname[k] |= QUOTE;
			if (gmatch(dname, gp)) {
				name = generate(we, pp, dname, np);
				if (*np && !strpbrk(spcl, np)) {
					if (stat(name,&dbuf)) {
						free(name);
						continue;
					}
				}
				nl = addword(name, nl);
			}
		}
	}
	close(dn);
	free(dp);
	free(gp);
}

/*
 * generate a pathname as below.
 * start..end1 / middle end
 * the slashes come for free
 */
static char *
generate(char *start1, char *end1, char *middle, char *end)
{
	char *p;
	char *op, *xp;

	p = op = space((int)(end1-start1)+strlen(middle)+strlen(end)+2);
	for (xp = start1; xp != end1;)
		*op++ = *xp++;
	for (xp = middle; (*op++ = *xp++) != '\0';)
		;
	op--;
	for (xp = end; (*op++ = *xp++) != '\0';)
		;
	return(p);
}

static int
anyspcl(struct wdblock *wb)
{
	int i;
	char **wd;

	wd = wb->w_words;
	for (i=0; i<wb->w_nword; i++)
		if (strpbrk(*wd++, spcl))
			return(1);
	return(0);
}

/* -------- word.c -------- */
/* #include "word.h" */

#define	NSTART	16	/* default number of words to allow for initially */

struct wdblock *
newword(int nw)
{
	struct wdblock *wb;

	wb = (struct wdblock *) space(sizeof(*wb) + nw*sizeof(char *));
	wb->w_bsize = nw;
	wb->w_nword = 0;
	return(wb);
}

struct wdblock *
addword(char *wd, struct wdblock *wb)
{
	struct wdblock *wb2;
	int nw;

	if (wb == NULL)
		wb = newword(NSTART);
	if ((nw = wb->w_nword) >= wb->w_bsize) {
		wb2 = newword(nw * 2);
		memcpy(wb2->w_words, wb->w_words, nw*sizeof(char *));
		wb2->w_nword = nw;
		free(wb);
		wb = wb2;
	}
	wb->w_words[wb->w_nword++] = (wd);
	return(wb);
}

char **
getwords(struct wdblock *wb)
{
	char **wd;
	int nb;

	if (wb == NULL)
		return((char **)NULL);
	if (wb->w_nword == 0) {
		free(wb);
		return((char **)NULL);
	}
	wd = (char **) space(nb = sizeof(*wd) * wb->w_nword);
	memcpy(wd, wb->w_words, nb);
	free(wb);	/* perhaps should done by caller */
	return(wd);
}

static void
glob0(char **a0, unsigned a1)
{
	glob1(a0, a0 + a1 * sizeof(char *));
}

static void
glob1(char **base, char **lim)
{
	char **i, **j;
	char **lptr, **hptr;
	int c;
	unsigned n;
top:
	if ((n=(int)(lim-base)) <= 1)
		return;
	n = n / 2;
	hptr = lptr = base+n;
	i = base;
	j = lim-1;
	for(;;) {
		if (i < lptr) {
			if ((c = strcmp(*i, *lptr)) == 0) {
				glob2(i, lptr -= 1);
				continue;
			}
			if (c < 0) {
				i += 1;
				continue;
			}
		}

begin:
		if (j > hptr) {
			if ((c = strcmp(*hptr, *j)) == 0) {
				glob2(hptr += 1, j);
				goto begin;
			}
			if (c > 0) {
				if (i == lptr) {
					glob3(i, hptr += 1, j);
					i = lptr += 1;
					goto begin;
				}
				glob2(i, j);
				j -= 1;
				i += 1;
				continue;
			}
			j -= 1;
			goto begin;
		}


		if (i == lptr) {
			if (lptr-base >= lim-hptr) {
				glob1(hptr+1, lim);
				lim = lptr;
			} else {
				glob1(base, lptr);
				base = hptr+1;
			}
			goto top;
		}


		glob3(j, lptr -= 1, i);
		j = hptr -= 1;
	}
}

static void
glob2(char **i, char **j)
{
	char **index1, **index2, *c;
	index1 = i;
	index2 = j;
		c = *index1;
		*index1++ = *index2;
		*index2++ = c;
}

static void
glob3(char **i, char **j, char **k)
{
	char **index1, **index2, **index3;
	char *c;
	index1 = i;
	index2 = j;
	index3 = k;
		c = *index1;
		*index1++ = *index3;
		*index3++ = *index2;
		*index2++ = c;
}


/* -------- io.c -------- */

/*
 * shell IO
 */

static struct iobuf sharedbuf = {AFID_NOBUF};
static struct iobuf mainbuf = {AFID_NOBUF};
static unsigned bufid = AFID_ID;	/* buffer id counter */

static void readhere (char **name, int *tf, char *s, int ec );
static void pushio (struct ioarg *argp, int (*fn)(struct ioarg *, struct io *));
static int xxchar (struct ioarg *ap, struct io *iop );

static int
mygetc(int ec)
{
	int c;

	if(e.linep > elinep) {
		while((c=readc()) != '\n' && c)
			;
		sh_errf("input line too long\n");
		gflg++;
		return(c);
	}
	c = readc();
 	if (ec != '\'' && e.iop->task != XGRAVE) {
		if(c == '\\') {
			c = readc();
			if (c == '\n' && ec != '\"')
				return(mygetc(ec));
			c |= QUOTE;
		}
	}
	return(c);
}

static void
unget(int c)
{
	if (e.iop >= e.iobase)
		e.iop->peekc = c;
}

static int
eofc(void)

{
  return e.iop < e.iobase || (e.iop->peekc == 0 && e.iop->prev == 0);
}

static int
readc(void)
{
	int c;

	for (; e.iop >= e.iobase; e.iop--)
		if ((c = e.iop->peekc) != '\0') {
			e.iop->peekc = 0;
			return(c);
		}
		else {
		    if (e.iop->prev != 0) {
		        if ((c = (*e.iop->iofn)(e.iop->argp, e.iop)) != '\0') {
			        if (c == -1) {
				        e.iop++;
				        continue;
			        }
			        if (e.iop == iostack)
				        ioecho(c);
			        return(e.iop->prev = c);
		        }
		        else if (e.iop->task == XIO && e.iop->prev != '\n') {
			        e.iop->prev = 0;
				if (e.iop == iostack)
					ioecho('\n');
			        return '\n';
		        }
		    }
		    if (e.iop->task == XIO) {
			if (multiline)
			    return e.iop->prev = 0;
			if (talking && e.iop == iostack+1)
			    prs(prompt->value);
		    }
		}
	if (e.iop >= iostack)
		return(0);
	leave();
	/* NOTREACHED */
}

static void
ioecho(int c)
{
	if (flag['v'])
		fputc(c, stderr);
}

static void
pushio(struct ioarg *argp, int (*fn)(struct ioarg *, struct io *))
{
	if (++e.iop >= &iostack[NPUSH]) {
		e.iop--;
		sh_errf("Shell input nested too deeply\n");
		gflg++;
		return;
	}
	e.iop->iofn = (int(*)(struct ioarg *, struct io *))fn;

	if (argp->afid != AFID_NOBUF)
	  e.iop->argp = argp;
	else {
	  e.iop->argp  = ioargstack + (e.iop - iostack);
	  *e.iop->argp = *argp;
	  e.iop->argp->afbuf = e.iop == &iostack[0] ? &mainbuf : &sharedbuf;
	  if (isatty(e.iop->argp->afile) == 0 &&
	      (e.iop == &iostack[0] ||
	       lseek(e.iop->argp->afile, 0L, 1) != -1)) {
	    if (++bufid == AFID_NOBUF)
	      bufid = AFID_ID;
	    e.iop->argp->afid  = bufid;
	  }
	}

	e.iop->prev  = ~'\n';
	e.iop->peekc = 0;
	e.iop->xchar = 0;
	e.iop->nlcount = 0;
	if (fn == filechar || fn == linechar)
		e.iop->task = XIO;
	else if (fn == gravechar || fn == qgravechar)
		e.iop->task = XGRAVE;
	else
		e.iop->task = XOTHER;
}

struct io *
setbase(struct io *ip)
{
	struct io *xp;

	xp = e.iobase;
	e.iobase = ip;
	return(xp);
}

/*
 * Input generating functions
 */

/*
 * Produce the characters of a string, then a newline, then EOF.
 */
static int
nlchar(struct ioarg *ap, struct io *iop)
{
	int c;

	if (ap->aword == NULL)
		return(0);
	if ((c = *ap->aword++) == 0) {
		ap->aword = NULL;
		return('\n');
	}
	return(c);
}

/*
 * Given a list of words, produce the characters
 * in them, with a space after each word.
 */
static int
wdchar(struct ioarg *ap, struct io *iop)
{
	char c;
	char **wl;

	if ((wl = ap->awordlist) == NULL)
		return(0);
	if (*wl != NULL) {
		if ((c = *(*wl)++) != 0)
			return(c & 0177);
		ap->awordlist++;
		return(' ');
	}
	ap->awordlist = NULL;
	return('\n');
}

/*
 * Return the characters of a list of words,
 * producing a space between them.
 */
static int
dolchar(struct ioarg *ap, struct io *iop)
{
	char *wp;

	if ((wp = *ap->awordlist++) != NULL) {
		PUSHIO(aword, wp, *ap->awordlist == NULL? strchar: xxchar);
		return(-1);
	}
	return(0);
}

static int
xxchar(struct ioarg *ap, struct io *iop)
{
	int c;

	if (ap->aword == NULL)
		return(0);
	if ((c = *ap->aword++) == '\0') {
		ap->aword = NULL;
		return(' ');
	}
	return(c);
}

/*
 * Produce the characters from a single word (string).
 */
static int
strchar(struct ioarg *ap, struct io *iop)
{
	int c;

	if (ap->aword == NULL || (c = *ap->aword++) == 0)
		return(0);
	return(c);
}

/*
 * Produce quoted characters from a single word (string).
 */
static int
qstrchar(struct ioarg *ap, struct io *iop)
{
	int c;

	if (ap->aword == NULL || (c = *ap->aword++) == 0)
		return(0);
	return(c|QUOTE);
}

/*
 * Return the characters from a file.
 */
static int
filechar(struct ioarg *ap, struct io *iop)
{
	int i;
	char c;
	struct iobuf *bp = ap->afbuf;

	if (ap->afid != AFID_NOBUF) {
	  if ((i = (ap->afid != bp->id)) || bp->bufp == bp->ebufp) {
	    if (i)
	      lseek(ap->afile, ap->afpos, 0);
	    do {
	      i = read(ap->afile, bp->buf, sizeof(bp->buf));
	    } while (i < 0 && errno == EINTR);
	    if (i <= 0) {
	      closef(ap->afile);
	      return 0;
	    }
	    bp->id = ap->afid;
	    bp->ebufp = (bp->bufp  = bp->buf) + i;
	  }
	  ap->afpos++;
	  return *bp->bufp++ & 0177;
	}

	do {
		i = read(ap->afile, &c, sizeof(c));
	} while (i < 0 && errno == EINTR);
	return(i == sizeof(c)? c&0177: (closef(ap->afile), 0));
}

/*
 * Return the characters from a here temp file.
 */
static int
herechar(struct ioarg *ap, struct io *iop)
{
	char c;


	if (read(ap->afile, &c, sizeof(c)) != sizeof(c)) {
		close(ap->afile);
		c = 0;
	}
	return (c);

}

/*
 * Return the characters produced by a process (`...`).
 * Quote them if required, and remove any trailing newline characters.
 */
static int
gravechar(struct ioarg *ap, struct io *iop)
{
	int c;

	if ((c = qgravechar(ap, iop)&~QUOTE) == '\n')
		c = ' ';
	return(c);
}

static int
qgravechar(struct ioarg *ap, struct io *iop)
{
	int c;

	if (iop->xchar) {
		if (iop->nlcount) {
			iop->nlcount--;
			return('\n'|QUOTE);
		}
		c = iop->xchar;
		iop->xchar = 0;
	} else if ((c = filechar(ap, iop)) == '\n') {
		iop->nlcount = 1;
		while ((c = filechar(ap, iop)) == '\n')
			iop->nlcount++;
		iop->xchar = c;
		if (c == 0)
			return(c);
		iop->nlcount--;
		c = '\n';
	}
	return(c!=0? c|QUOTE: 0);
}

/*
 * Return a single command (usually the first line) from a file.
 */
static int
linechar(struct ioarg *ap, struct io *iop)
{
	int c;

	if ((c = filechar(ap, iop)) == '\n') {
		if (!multiline) {
			closef(ap->afile);
			ap->afile = -1;	/* illegal value */
		}
	}
	return(c);
}

static void
prs(char *s)
{
	if (*s)
		fputs(s, stderr);
}

static void
closef(int i)
{
	if (i > 2)
		close(i);
}

static void
closeall(void)
{
	int u;

	for (u = NUFILE; u < NOFILE_;)
		close(u++);
}

/*
 * remap fd into Shell's fd space
 */
static int
remap(int fd)
{
	int i;
	int map[NOFILE_];

	if (fd < e.iofd) {
		for (i = 0; i < NOFILE_; i++)
			map[i] = 0;
		do {
			map[fd] = 1;
			fd = dup(fd);
		} while (fd >= 0 && fd < e.iofd);
		for (i = 0; i < NOFILE_; i++)
			if (map[i])
				close(i);
		if (fd < 0)
			sh_errf("too many files open in shell\n");
	}
	return(fd);
}

static int
openpipe(int *pv)
{
	int i;

	if ((i = pipe(pv)) < 0)
		sh_errf("can't create pipe - try again\n");
	return(i);
}

static void
closepipe(int *pv)
{
	if (pv != NULL) {
		close(*pv++);
		close(*pv);
	}
}

/* -------- here.c -------- */

static	struct here *inhere;		/* list of hear docs while parsing */
static	struct here *acthere;		/* list of active here documents */

static void
inithere(void)
{
	inhere=acthere=(struct here*)0;
}

static void
markhere(char *s, struct ioword *iop)
{
	struct here *h, *lh;

	h = (struct here *) malloc(sizeof(struct here));
	if (h == 0)
		return;
	h->h_tag = evalstr(s, DOSUB);
	if (h->h_tag == 0)
		return;
	h->h_iop = iop;
	iop->io_name = 0;
	h->h_next = NULL;
	if (inhere == 0)
		inhere = h;
	else
		for (lh = inhere; lh!=NULL; lh = lh->h_next)
			if (lh->h_next == 0) {
				lh->h_next = h;
				break;
			}
	iop->io_flag |= IOHERE|IOXHERE;
	for (s = h->h_tag; *s; s++)
		if (*s & QUOTE) {
			iop->io_flag &= ~ IOXHERE;
			*s &= ~ QUOTE;
		}
	h->h_dosub = iop->io_flag & IOXHERE;
}

static void
gethere(void)
{
	struct here *h, *hp;

	/* Scan here files first leaving inhere list in place */
	for (hp = h = inhere; h != NULL; hp = h, h = h->h_next)
	  readhere(&h->h_iop->io_name, &h->h_iop->io_fd, h->h_tag, h->h_dosub? 0: '\'');

	/* Make inhere list active - keep list intact for scraphere */
	if (hp != NULL) {
	  hp->h_next = acthere;
	  acthere    = inhere;
	  inhere     = NULL;
	}
}

static void
readhere(char **name, int *tf, char *s, int ec)
{
	char tname[] = "/tmp/XXXXXX";
	int c;
	jmp_buf ev;
	char line [LINELIM+1];
	char *next;

	*tf = mkstemp(tname);
	*name = strsave(tname, areanum);
	if (*tf < 0)
		return;
	if (newenv(setjmp(errpt = ev)) != 0)
		unlink(tname);
	else {
		pushio(e.iop->argp, e.iop->iofn);
		e.iobase = e.iop;
		for (;;) {
			if (talking && e.iop <= iostack)
				prs(cprompt->value);
			next = line;
			while ((c = readc()) != '\n' && c) {
				if (next >= &line[LINELIM]) {
					c = 0;
					break;
				}
				*next++ = c;
			}
			*next = 0;
			if (strcmp(s, line) == 0 || c == 0)
				break;
			*next++ = '\n';
			write (*tf, line, (int)(next-line));
		}
		if (c == 0) {
			sh_errf("here document `%s' unclosed\n", s);
		}
		quitenv();
	}
	close(*tf);
}

/*
 * open here temp file.
 * if unquoted here, expand here temp file into second temp file.
 */
static int
herein(char *hname, int xdoll)
{
	int hf, tf;

	if (hname == 0)
		return(-1);
	hf = open(hname, O_RDONLY);
	if (hf < 0)
		return (-1);
	if (xdoll) {
		char c;
		char tname[] = "/tmp/XXXXXX";
		jmp_buf ev;

		if ((tf = mkstemp(tname)) < 0)
			return (-1);
		if (newenv(setjmp(errpt = ev)) == 0) {
			PUSHIO(afile, hf, herechar);
			setbase(e.iop);
			while ((c = subgetc(0, 0)) != 0) {
				char c1 = c&~QUOTE;

				if (c&QUOTE && !strchr("`$\\", c1))
					write(tf,"\\",1);
				write(tf, &c1, 1);
			}
			quitenv();
		} else
			unlink(tname);
		close(tf);
		tf = open(tname, O_RDONLY);
		unlink(tname);
		return (tf);
	} else
		return (hf);
}

static void
scraphere(void)
{
	struct here *h;

	for (h = inhere; h != NULL; h = h->h_next) {
		if (h->h_iop && h->h_iop->io_name)
			unlink(h->h_iop->io_name);
	}
	inhere = NULL;
}

static void
freehere(int area)
{
	struct here *h, *hl;

	hl = NULL;
	for (h = acthere; h != NULL; h = h->h_next)
		if (getarea((char *) h) >= area) {
			if (h->h_iop->io_name)
				unlink(h->h_iop->io_name);
			if (hl == NULL)
				acthere = h->h_next;
			else
				hl->h_next = h->h_next;
		} else
			hl = h;
}
