/*
 * Copyright (c) 1995 Gordon W. Ross
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _XOPEN_SOURCE 500

#include "extern.h"

#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>

#include <dirent.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

static int iflag;

static void show_long(const char *);

int
ls_main(int argc, char *argv[])
{
	DIR *dfp;
	struct dirent *d;

	/* If given an arg, just cd there first. */
	if (argc > 1) {
		if (chdir(argv[1]))
			err(1, "chdir `%s'", argv[1]);
	}
	if (argc > 2)
		fprintf(stderr, "extra args ignored\n");

	dfp = opendir(".");
	if (dfp == NULL)
		err(EXIT_FAILURE, "opendir");

	while ((d = readdir(dfp)) != NULL)
		show_long(d->d_name);

	closedir(dfp);
	return EXIT_SUCCESS;
}

static void
show_long(const char *fname)
{
	struct stat st;
	if (lstat(fname, &st)) {
		warn("lstat `%s'", fname);
		return;
	}

	char ifmt_c = '?';
	switch(st.st_mode & S_IFMT) {
	case S_IFDIR:
		ifmt_c = 'D';
		break;
	case S_IFCHR:
		ifmt_c = 'C';
		break;
	case S_IFBLK:
		ifmt_c = 'B';
		break;
	case S_IFREG:
		ifmt_c = 'F';
		break;
	case S_IFIFO:
		ifmt_c = 'P';
		break;
	case S_IFLNK:
		ifmt_c = 'L';
		break;
	case S_IFSOCK:
		ifmt_c = 'S';
		break;
#ifdef S_IFWHT
	case S_IFWHT:
		ifmt_c = 'W';
		break;
#endif
	}

	if (iflag) {
		/* inode number */
		printf("%10ld ",  (long)st.st_ino);
	}

	/* fmt/mode */
	printf("%c:",   ifmt_c);
	printf("%04o ", st.st_mode & ~S_IFMT);

	/* nlinks, uid, gid */
	printf("%2llu ", (unsigned long long)st.st_nlink);
	printf("%4d ",  st.st_uid);
	printf("%4d ",  st.st_gid);

	/* size or major/minor */
	if (ifmt_c == 'B' || ifmt_c == 'C') {
		printf("%4d, ",  major(st.st_rdev));
		printf("%5d ",   minor(st.st_rdev));
	} else {
		printf("%11ld ", (long) st.st_size);
	}

	/* date */
	char *date = ctime(&st.st_mtime);
	if (date)
		printf("%.12s ", date + 4);
	else
		printf("? ");

	/* name */
	printf("%s", fname);

	if (ifmt_c == 'L') {
		char linkto[MAXPATHLEN];
		int n = readlink(fname, linkto, sizeof(linkto));
		if (n < 0) {
			warn("readlink `%s'", fname);
			return;
		}
		printf(" -> %.*s", n, linkto);
	}
	printf("\n");
}
