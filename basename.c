/*
 * Copyright (c) 2021 Matija Skala
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define _POSIX_C_SOURCE 200809L

#include "extern.h"

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void
usage(void)
{
	fprintf(stderr, "usage: basename string [suffix]\n");
	exit(1);
}

char *
do_basename(const char *path, const char *suffix, size_t *out_length)
{
	if (!path || !*path) {
		if (out_length)
			*out_length = 0;
		return NULL;
	}
	size_t last = strlen(path) - 1;
	while (last && path[last] == '/')
		last--;
	size_t first = last;
	while (first && path[first-1] != '/')
		first--;
	if (suffix && *suffix) {
		size_t n = strnlen(suffix, last - first + 1);
		if (n < last - first + 1 && !memcmp(path + last + 1 - n, suffix, n))
			last -= n;
	}
	if (out_length)
		*out_length = last - first + 1;
	return (char*)path + first;
}

int basename_main(int argc, char *argv[])
{
	if (argc < 2 || argc > 3)
		usage();
	const char *suffix = argc == 3 ? argv[2] : NULL;
	size_t n;
	const char *s = do_basename(argv[1], suffix, &n);
	printf("%.*s\n", (int)n, s);
	return EXIT_SUCCESS;
}
