/*
 * Copyright 2021 Matija Skala <mskala@gmx.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _XOPEN_SOURCE 700

#include "extern.h"

#include <err.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

static char *
combine_strings(char *dest, char c, const char *src, size_t *n)
{
	size_t destlen = strlen(dest), srclen = strlen(src);
	if (*n < destlen + srclen + 2 && !(dest = realloc(dest, *n += destlen + srclen + 2)))
		err(2, NULL);
	dest[destlen] = c;
	memcpy(dest + destlen + 1, src, srclen + 1);
	return dest;
}

static char **
lcs_c(char **a, int a_length, char **b, int b_length, int start_index, size_t *n)
{
	if (a_length > 2048) {
		int t1 = 0, t2 = 0;
		int acut = a_length/2;
		int *tmp1 = malloc(b_length * sizeof *tmp1);
		int *tmp2 = malloc(b_length * sizeof *tmp2);
		for (int i = 0; i < acut; i++)
			for (int j = 0; j < b_length; j++) {
				int t = t1;
				t1 = tmp1[j];
				if (!strcmp(a[i], b[j]))
					tmp1[j] = (i == 0 || j == 0) ? 1 : (t + 1);
				else if (i == 0 || (j != 0 && tmp1[j] < tmp1[j-1]))
					tmp1[j] = j ? tmp1[j-1] : 0;
			}
		for (int i = a_length; i > acut; i--)
			for (int j = b_length; j > 0; j--) {
				int t = t2;
				t2 = tmp2[j-1];
				if (!strcmp(a[i-1], b[j-1]))
					tmp2[j-1] = (i == a_length || j == a_length) ? 1 : (t + 1);
				else if (i == 0 || (j != 0 && tmp2[j-1] < tmp2[j]))
					tmp2[j-1] = j ? tmp2[j] : 0;
			}
		int max = -1;
		for (int i = 0; i < b_length; i++)
			if ((max == -1 ? 0 : tmp1[max]) + tmp2[max+1] < tmp1[i] + (i+1 == b_length ? 0 : tmp2[i]))
				max = i;
		free(tmp1);
		free(tmp2);
		size_t n1, n2;
		char **lcs1 = lcs_c(a, acut, b, max+1, start_index, &n1);
		char **lcs2 = lcs_c(a+acut, a_length-acut, b+max+1, b_length-max-1, start_index+acut, &n2);
		char **r = malloc((n1 + n2) * sizeof *r);
		*n = n1 + n2;
		memcpy(r, lcs1, n1 * sizeof *r);
		free(lcs1);
		memcpy(r+n1, lcs2, n2 * sizeof *r);
		free(lcs2);
		return r;
	}
	int8_t *tmp1 = malloc(a_length * b_length * sizeof *tmp1);
	size_t *tmp2 = malloc(a_length * b_length * sizeof *tmp2);
	for (int i = 0; i < a_length; i++)
		for (int j = 0; j < b_length; j++) {
			if (!strcmp(a[i], b[j]))
				tmp1[i*b_length+j] = 0;
			else if (i == 0 || j == 0)
				tmp1[i*b_length+j] = i == 0 ? 1 : 2;
			else
				tmp1[i*b_length+j] = tmp2[(i-1)*b_length+j] < tmp2[i*b_length+j-1] ? 1 : 2;
			switch (tmp1[i*b_length+j]) {
				case 0:
					tmp2[i*b_length+j] = (i == 0 || j == 0) ? 1 : (tmp2[(i-1)*b_length+j-1] + 1);
					break;
				case 1:
					tmp2[i*b_length+j] = j ? tmp2[i*b_length+j-1] : 0;
					break;
				case 2:
					tmp2[i*b_length+j] = tmp2[(i-1)*b_length+j];
					break;
			}
		}
	size_t j = a_length*b_length-1;
	size_t i = tmp2[j]*2;
	char **r = malloc(i * sizeof *r);
	*n = i;
	while (i)
		switch (tmp1[j]) {
			case 0:
				r[--i] = b[j%b_length];
				r[--i] = a[j/b_length];
				j -= b_length+1;
				break;
			case 1:
				j -= 1;
				break;
			case 2:
				j -= b_length;
				break;
		}
	free(tmp1);
	free(tmp2);
	return r;
}

int
diff_main(int argc, char *argv[])
{
	FILE *f1 = fopen(argv[1], "rb");
	FILE *f2 = fopen(argv[2], "rb");
	char **a = malloc(sizeof *a);
	char **b = malloc(sizeof *b);
	size_t i = 0, j = 0;
	char *buf = NULL;
	size_t len = 0;
	while (getline(&buf, &len, f1) != -1) {
		a[i++] = strdup(buf);
		if (!(i & (i+1))) {
			a = realloc(a, 2 * (i+1) * sizeof *a);
			if (!a)
				err(2, NULL);
		}
	}
	while (getline(&buf, &len, f2) != -1) {
		b[j++] = strdup(buf);
		if (!(j & (j+1))) {
			b = realloc(b, 2 * (j+1) * sizeof *b);
			if (!b)
				err(2, NULL);
		}
	}
	size_t n;
	char **cs = lcs_c(a, i, b, j, 0, &n);
	size_t a_length = i, b_length = j;
	i = j = 0;
	char *s = calloc(1, 1);
	len = 1;
	long beg1 = 0;
	long beg2 = 0;
	long linec1 = 0;
	long linec2 = 0;
	size_t ci = 0;
	int rval = 0;
	while (a[i] && b[j]) {
		int fast_forward = 1;
		for (int k = 0; k < 7 && i+k < a_length; k++)
			if (a[i+k] != cs[2*(ci+k)])
				fast_forward = 0;
		for (int k = 0; k < 7 && j+k < b_length; k++)
			if (b[j+k] != cs[2*(ci+k)+1])
				fast_forward = 0;
		if (fast_forward || (!i && !j)) {
			if (linec1 || linec2) {
				for (int k = 0; k < 3 && i+k < a_length; k++) {
					s = combine_strings(s, ' ', a[i+k], &len);
					i++;
					j++;
					ci++;
					linec1++;
					linec2++;
				}
				if (!rval)
					printf("--- %s\n+++ %s\n", argv[1], argv[2]);
				rval = 1;
				printf("@@ -%ld,%ld +%ld,%ld @@\n%s", beg1, linec1, beg2, linec2, s);
				*s = 0;
			}
			while (i < a_length && j < b_length && ci <= n/2 && a[i] == cs[2*ci] && b[j] == cs[2*ci+1]) {
				i++;
				j++;
				ci++;
			}
			if (i == a_length && j == b_length)
				break;
			beg1 = i < 3 ? 1 : i-2;
			beg2 = j < 3 ? 1 : j-2;
			if (i - beg1 != j - beg2)
				errx(2, "internal error");
			linec1 = linec2 = 0;
			for (size_t k = beg1-1; k < i; k++) {
				s = combine_strings(s, ' ', a[k], &len);
				linec1++;
				linec2++;
			}
		}
		while (i < a_length && j < b_length && ci <= n/2 && a[i] == cs[2*ci] && b[j] == cs[2*ci+1]) {
			s = combine_strings(s, ' ', a[i], &len);
			i++;
			j++;
			ci++;
			linec1++;
			linec2++;
		}
		while (i < a_length && ci <= n/2 && a[i] != cs[2*ci]) {
			s = combine_strings(s, '-', a[i], &len);
			i++;
			linec1++;
		}
		while (j < b_length && ci <= n/2 && b[j] != cs[2*ci+1]) {
			s = combine_strings(s, '+', b[j], &len);
			j++;
			linec2++;
		}
	}
	if (*s) {
		if (!rval)
			printf("--- %s\n+++ %s\n", argv[1], argv[2]);
		rval = 1;
		printf("@@ -%ld,%ld +%ld,%ld @@\n%s", beg1, linec1, beg2, linec2, s);
	}
	free(a);
	free(b);
	free(cs);
	free(s);

	return rval;
}
