/*
 * Copyright (c) 2021 Matija Skala
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "extern.h"

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main(int argc, char *argv[])
{
	extern char *__progname;
	size_t n;
#ifdef _WIN32
	__progname = do_basename(argv[0], ".exe", &n);
#else
	__progname = do_basename(argv[0], NULL, &n);
#endif
#define called_as(APPNAME) \
	(n == strlen(APPNAME) && !memcmp(__progname, APPNAME, n))
	if (called_as("tinybox")) {
		argc--;
		argv++;
		if (argc == 0) {
			puts("[ basename cat cp diff dirname false gzip id kilo ls mv pwd realpath rm rmdir sed sh sum tar test true unlink zcat");
			return EXIT_SUCCESS;
		}
		__progname = argv[0];
		n = strlen(argv[0]);
	}
	if (*__progname == '[')
		return test_main(argc, argv);
	if (called_as("true"))
		return 0;
	if (called_as("false"))
		return 1;
	if (called_as("unlink"))
		return rm_main(argc, argv);
	if (called_as("zcat"))
		return cat_main(argc, argv);
#define APPLET(APP) \
	if (called_as(#APP)) \
		return APP##_main(argc, argv);
	APPLET(basename)
	APPLET(cat)
	APPLET(cp)
	APPLET(diff)
	APPLET(dirname)
	APPLET(gzip)
	APPLET(id)
	APPLET(kilo)
	APPLET(ls)
	APPLET(mv)
	APPLET(pwd)
	APPLET(realpath)
	APPLET(rm)
	APPLET(rmdir)
	APPLET(sed)
	APPLET(sh)
	APPLET(sum)
	APPLET(tar)
	APPLET(test)
	errx(EXIT_FAILURE, "applet not found");
}
