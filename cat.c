/*
 * Copyright (c) 2021 Matija Skala
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "extern.h"

#include <err.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <zlib.h>

int
cat_main(int argc, char *argv[])
{
	extern char *__progname;
	int zcat = *__progname == 'z';
	int fd = STDIN_FILENO, rval = EXIT_SUCCESS;
	do {
		if (--argc && (fd = open(*++argv, O_RDONLY)) < 0) {
			warn("%s", *argv);
			rval = EXIT_FAILURE;
			continue;
		}
		const char *filename = fd == STDIN_FILENO ? "stdin" : *argv;
		gzFile gzfp = NULL;
		if (zcat && !gzdopen(fd, "rb")) {
			warn("%s", filename);
			rval = EXIT_FAILURE;
			continue;
		}
		for (;;) {
			char buf[BUFSIZ];
			ssize_t n = zcat ? gzread(gzfp, buf, sizeof buf) : read(fd, buf, sizeof buf);
			if (n < 0) {
				warn("%s", filename);
				rval = EXIT_FAILURE;
			}
			if (n <= 0)
				break;
			if (fwrite(buf, 1, n, stdout) != (size_t)n)
				err(EXIT_FAILURE, NULL);
		}
		if (zcat ? gzclose(gzfp) < 0 : close(fd) < 0)
			err(EXIT_FAILURE, "%s", filename);
	} while (argc > 1);
	return rval;
}
