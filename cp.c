/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 1988, 1991, 1993, 1994
 *	The Regents of the University of California.  All rights reserved.
 *
 * This code is derived from software contributed to Berkeley by
 * David Hitz of Auspex Systems Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#if 0
#ifndef lint
static char const copyright[] =
"@(#) Copyright (c) 1988, 1993, 1994\n\
	The Regents of the University of California.  All rights reserved.\n";
#endif /* not lint */

#ifndef lint
static char sccsid[] = "@(#)cp.c	8.2 (Berkeley) 4/1/94";
#endif /* not lint */
#endif

/*
 * Cp copies source files to target files.
 *
 * The global PATH_T structure "to" always contains the path to the
 * current target file.  Since fts(3) does not change directories,
 * this path can be either absolute or dot-relative.
 *
 * The basic algorithm is to initialize "to" and use fts(3) to traverse
 * the file hierarchy rooted in the argument list.  A trivial case is the
 * case of 'cp file1 file2'.  The more interesting case is the case of
 * 'cp file1 file2 ... fileN dir' where the hierarchy is traversed and the
 * path (relative to the root of the traversal) is appended to dir (stored
 * in "to") to form the final target path.
 */

#define _GNU_SOURCE

#include "extern.h"

#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <ftw.h>
#include <getopt.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "extern.h"

#define	STRIP_TRAILING_SLASH(p) {					\
	while ((p).p_end > (p).p_path + 1 && (p).p_end[-1] == '/')	\
	*--(p).p_end = 0;						\
}

static int	copy_fifo(const struct stat *, int);
static int	copy_file(const char *, const struct stat *, int);
static int	copy_link(const char *, const struct stat *, int);
static int	copy_special(const struct stat *, int);
static int	setfile(const struct stat *, int);
static void	usage(void);

typedef struct {
	char	*p_end;			/* pointer to NULL at end of path */
	char	*target_end;		/* pointer to end of target base */
	char	p_path[PATH_MAX];	/* pointer to the start of a path */
} PATH_T;

static char emptystring[] = "";

static PATH_T to = { to.p_path, emptystring, "" };

static int fflag, iflag, lflag, nflag, pflag, sflag, vflag;
static int Rflag, dflag, rflag;

static char *dirpath;
static int rval = 0;

static enum op { FILE_TO_FILE, FILE_TO_DIR, DIR_TO_DNE } type;

static int copy(const char *, const struct stat *, int, struct FTW *);

int
cp_main(int argc, char *argv[])
{
	struct stat to_stat, tmp_stat;
	int Hflag, Lflag, ch, ftw_options, r, have_trailing_slash;
	char *target;

	ftw_options = FTW_PHYS;
	Hflag = Lflag = 0;
	while ((ch = getopt(argc, argv, "HLPRadfilnprsvx")) != -1)
		switch (ch) {
		case 'H':
			Hflag = 1;
			Lflag = 0;
			break;
		case 'L':
			Lflag = 1;
			Hflag = 0;
			break;
		case 'P':
			Hflag = Lflag = 0;
			break;
		case 'R':
			Rflag = 1;
			break;
		case 'a':
			pflag = 1;
			Rflag = 1;
			Hflag = Lflag = 0;
			break;
		case 'd':
			dflag = 1;
			break;
		case 'f':
			fflag = 1;
			iflag = nflag = 0;
			break;
		case 'i':
			iflag = 1;
			fflag = nflag = 0;
			break;
		case 'l':
			lflag = 1;
			break;
		case 'n':
			nflag = 1;
			fflag = iflag = 0;
			break;
		case 'p':
			pflag = 1;
			break;
		case 'r':
			rflag = Lflag = 1;
			Hflag = 0;
			break;
		case 's':
			sflag = 1;
			break;
		case 'v':
			vflag = 1;
			break;
		case 'x':
			ftw_options |= FTW_MOUNT;
			break;
		default:
			usage();
		}
	argc -= optind;
	argv += optind;

	if (argc < 2)
		usage();

	if (Rflag && rflag)
		errx(1, "the -R and -r options may not be specified together");
	if (lflag && sflag)
		errx(1, "the -l and -s options may not be specified together");
	if (rflag)
		Rflag = 1;
	if (Rflag) {
		if (Lflag) {
			ftw_options &= ~FTW_PHYS;
		}
	} else if (!dflag) {
		ftw_options &= ~FTW_PHYS;
	}

	/* Save the target base in "to". */
	target = argv[--argc];
	if (memccpy(to.p_path, target, 0, sizeof(to.p_path)) == NULL)
		errx(1, "%s: name too long", target);
	to.p_end = to.p_path + strlen(to.p_path);
	if (to.p_path == to.p_end) {
		*to.p_end++ = '.';
		*to.p_end = 0;
	}
	have_trailing_slash = (to.p_end[-1] == '/');
	if (have_trailing_slash)
		STRIP_TRAILING_SLASH(to);
	to.target_end = to.p_end;

	/* Set end of argument list for fts(3). */
	argv[argc] = NULL;

	/*
	 * Cp has two distinct cases:
	 *
	 * cp [-R] source target
	 * cp [-R] source1 ... sourceN directory
	 *
	 * In both cases, source can be either a file or a directory.
	 *
	 * In (1), the target becomes a copy of the source. That is, if the
	 * source is a file, the target will be a file, and likewise for
	 * directories.
	 *
	 * In (2), the real target is not directory, but "directory/source".
	 */
	r = stat(to.p_path, &to_stat);
	if (r == -1 && errno != ENOENT)
		err(1, "%s", to.p_path);
	if (r == -1 || !S_ISDIR(to_stat.st_mode)) {
		/*
		 * Case (1).  Target is not a directory.
		 */
		if (argc > 1)
			errx(1, "%s is not a directory", to.p_path);

		/*
		 * Need to detect the case:
		 *	cp -R dir foo
		 * Where dir is a directory and foo does not exist, where
		 * we want pathname concatenations turned on but not for
		 * the initial mkdir().
		 */
		if (r == -1) {
			if (Rflag && (Lflag || Hflag))
				stat(*argv, &tmp_stat);
			else
				lstat(*argv, &tmp_stat);

			if (S_ISDIR(tmp_stat.st_mode) && Rflag)
				type = DIR_TO_DNE;
			else
				type = FILE_TO_FILE;
		} else
			type = FILE_TO_FILE;

		if (have_trailing_slash && type == FILE_TO_FILE) {
			if (r == -1) {
				errx(1, "directory %s does not exist",
				    to.p_path);
			} else
				errx(1, "%s is not a directory", to.p_path);
		}
	} else
		/*
		 * Case (2).  Target is a directory.
		 */
		type = FILE_TO_DIR;

	while (*argv) {
		struct stat st;
		if (lstat(*argv, &st)) {
			warn("%s", *argv++);
			continue;
		}
		if (!S_ISLNK(st.st_mode) || (Rflag ? Lflag || Hflag : !dflag)) {
			dirpath = realpath(*argv, NULL);
			nftw(dirpath, copy, 8, ftw_options);
			free(dirpath);
		}
		else {
			struct FTW ftwbuf = {0,0};
			dirpath = *argv;
			copy(*argv, &st, FTW_SL, &ftwbuf);
		}
		argv++;
	}
	return rval;
}

static int
copy(const char *path, const struct stat *st, int typeflag, struct FTW *ftwbuf)
{
	struct stat to_stat;
	int base = 0, dne, badcp = 0;
	size_t nlen;
	const char *p;
	char *target_mid;
	mode_t mask, mode;

	/*
	 * Need to remember the roots of traversals to create
	 * correct pathnames.  If there's a directory being
	 * copied to a non-existent directory, e.g.
	 *	cp -R a/dir noexist
	 * the resulting path name should be noexist/foo, not
	 * noexist/dir/foo (where foo is a file in dir), which
	 * is the case where the target exists.
	 *
	 * Also, check for "..".  This is for correct path
	 * concatenation for paths ending in "..", e.g.
	 *	cp -R .. /tmp
	 * Paths ending in ".." are changed to ".".  This is
	 * tricky, but seems the easiest way to fix the problem.
	 *
	 * XXX
	 * Since the first level MUST be FTS_ROOTLEVEL, base
	 * is always initialized.
	 */
	if (type != DIR_TO_DNE) {
		p = strrchr(dirpath, '/');
		base = (p == NULL) ? 0 :
		    (int)(p - dirpath + 1);

		if (!strcmp(&dirpath[base],
		    ".."))
			base += 1;
	} else
		base = strlen(dirpath);

	/*
	 * Keep an inverted copy of the umask, for use in correcting
	 * permissions on created directories when not using -p.
	 */
	mask = ~umask(0777);
	umask(~mask);

		switch (typeflag) {
		case FTW_NS:
		case FTW_DNR:
			warnx("%s: not readable", path);
			badcp = rval = 1;
			return 0;
		default:
			;
		}

		/*
		 * If we are in case (2) or (3) above, we need to append the
		 * source name to the target name.
		 */
		if (type != FILE_TO_FILE) {
			p = &path[base];
			nlen = strlen(&path[base]);
			target_mid = to.target_end;
			if (*p != '/' && target_mid[-1] != '/')
				*target_mid++ = '/';
			*target_mid = 0;
			if (target_mid - to.p_path + nlen >= PATH_MAX) {
				warnx("%s%s: name too long (not copied)",
				    to.p_path, p);
				badcp = rval = 1;
				return 0;
			}
			(void)strncat(target_mid, p, nlen);
			to.p_end = target_mid + nlen;
			*to.p_end = 0;
			STRIP_TRAILING_SLASH(to);
		}

		if (typeflag == FTW_DP) {
		}

		/* Not an error but need to remember it happened. */
		if (stat(to.p_path, &to_stat) == -1)
			dne = 1;
		else {
			if (to_stat.st_dev == st->st_dev &&
			    to_stat.st_ino == st->st_ino) {
				warnx("%s and %s are identical (not copied).",
				    to.p_path, path);
				badcp = rval = 1;
				return 0;
			}
			if (!S_ISDIR(st->st_mode) &&
			    S_ISDIR(to_stat.st_mode)) {
				warnx("cannot overwrite directory %s with "
				    "non-directory %s",
				    to.p_path, path);
				badcp = rval = 1;
				return 0;
			}
			dne = 0;
		}

		switch (st->st_mode & S_IFMT) {
		case S_IFLNK:
			/* Catch special case of a non-dangling symlink. */
			if (typeflag != FTW_SL) {
				if (copy_file(path, st, dne))
					badcp = rval = 1;
			} else {	
				if (copy_link(path, st, !dne))
					badcp = rval = 1;
			}
			break;
		case S_IFDIR:
			if (!Rflag) {
				warnx("%s is a directory (not copied).",
				    path);
				badcp = rval = 1;
				return 1;
			}
			/*
			 * If the directory doesn't exist, create the new
			 * one with the from file mode plus owner RWX bits,
			 * modified by the umask.  Trade-off between being
			 * able to write the directory (if from directory is
			 * 555) and not causing a permissions race.  If the
			 * umask blocks owner writes, we fail.
			 */
			if (dne) {
				if (mkdir(to.p_path,
				    st->st_mode | S_IRWXU) < 0)
					err(1, "%s", to.p_path);
			} else if (!S_ISDIR(to_stat.st_mode)) {
				errno = ENOTDIR;
				err(1, "%s", to.p_path);
			}
			/*
			 * If -p is in effect, set all the attributes.
			 * Otherwise, set the correct permissions, limited
			 * by the umask.  Optimise by avoiding a chmod()
			 * if possible (which is usually the case if we
			 * made the directory).  Note that mkdir() does not
			 * honour setuid, setgid and sticky bits, but we
			 * normally want to preserve them on directories.
			 */
			if (pflag) {
				if (setfile(st, -1))
					rval = 1;
			} else {
				mode = st->st_mode;
				if ((mode & (S_ISUID | S_ISGID | S_ISVTX)) ||
				    ((mode | S_IRWXU) & mask) != (mode & mask))
					if (chmod(to.p_path, mode & mask) !=
					    0) {
						warn("chmod: %s", to.p_path);
						rval = 1;
					}
			}
			return 0;
		case S_IFBLK:
		case S_IFCHR:
			if (Rflag && !sflag) {
				if (copy_special(st, !dne))
					badcp = rval = 1;
			} else {
				if (copy_file(path, st, dne))
					badcp = rval = 1;
			}
			break;
		case S_IFSOCK:
			warnx("%s is a socket (not copied).",
			    path);
			break;
		case S_IFIFO:
			if (Rflag && !sflag) {
				if (copy_fifo(st, !dne))
					badcp = rval = 1;
			} else {
				if (copy_file(path, st, dne))
					badcp = rval = 1;
			}
			break;
		default:
			if (copy_file(path, st, dne))
				badcp = rval = 1;
			break;
		}
		if (vflag && !badcp)
			(void)printf("%s -> %s\n", path, to.p_path);
	return 0;
}

#define	cp_pct(x, y)	((y == 0) ? 0 : (int)(100.0 * (x) / (y)))

/*
 * Memory strategy threshold, in pages: if physmem is larger then this, use a 
 * large buffer.
 */
#define PHYSPAGES_THRESHOLD (32*1024)

/* Maximum buffer size in bytes - do not allow it to grow larger than this. */
#define BUFSIZE_MAX (2*1024*1024)

/*
 * Small (default) buffer size in bytes. It's inefficient for this to be
 * smaller than MAXPHYS.
 */
#define BUFSIZE_SMALL (128*1024)

static ssize_t
copy_fallback(int from_fd, int to_fd, char *buf, size_t bufsize)
{
	ssize_t rcount, wresid, wcount = 0;
	char *bufp;

	rcount = read(from_fd, buf, bufsize);
	if (rcount <= 0)
		return (rcount);
	for (bufp = buf, wresid = rcount; ; bufp += wcount, wresid -= wcount) {
		wcount = write(to_fd, bufp, wresid);
		if (wcount <= 0)
			break;
		if (wcount >= (ssize_t)wresid)
			break;
	}
	return (wcount < 0 ? wcount : rcount);
}

static int
copy_file(const char *path, const struct stat *st, int dne)
{
	static char *buf = NULL;
	static size_t bufsize;
	const struct stat *fs;
	ssize_t rcount;
	off_t wtotal;
	int ch, checkch, from_fd, rval, to_fd;
#ifdef VM_AND_BUFFER_CACHE_SYNCHRONIZED
	char *p;
#endif
	int use_copy_file_range = 1;

	from_fd = to_fd = -1;
	if (!lflag && !sflag &&
	    (from_fd = open(path, O_RDONLY, 0)) == -1) {
		warn("%s", path);
		return (1);
	}

	fs = st;

	/*
	 * If the file exists and we're interactive, verify with the user.
	 * If the file DNE, set the mode to be the from file, minus setuid
	 * bits, modified by the umask; arguably wrong, but it makes copying
	 * executables work right and it's been that way forever.  (The
	 * other choice is 666 or'ed with the execute bits on the from file
	 * modified by the umask.)
	 */
	if (!dne) {
#define YESNO "(y/n [n]) "
		if (nflag) {
			if (vflag)
				printf("%s not overwritten\n", to.p_path);
			rval = 1;
			goto done;
		} else if (iflag) {
			(void)fprintf(stderr, "overwrite %s? %s", 
			    to.p_path, YESNO);
			checkch = ch = getchar();
			while (ch != '\n' && ch != EOF)
				ch = getchar();
			if (checkch != 'y' && checkch != 'Y') {
				(void)fprintf(stderr, "not overwritten\n");
				rval = 1;
				goto done;
			}
		}

		if (fflag) {
			/*
			 * Remove existing destination file name create a new
			 * file.
			 */
			(void)unlink(to.p_path);
			if (!lflag && !sflag) {
				to_fd = open(to.p_path,
				    O_WRONLY | O_TRUNC | O_CREAT,
				    fs->st_mode & ~(S_ISUID | S_ISGID));
			}
		} else if (!lflag && !sflag) {
			/* Overwrite existing destination file name. */
			to_fd = open(to.p_path, O_WRONLY | O_TRUNC, 0);
		}
	} else if (!lflag && !sflag) {
		to_fd = open(to.p_path, O_WRONLY | O_TRUNC | O_CREAT,
		    fs->st_mode & ~(S_ISUID | S_ISGID));
	}

	if (!lflag && !sflag && to_fd == -1) {
		warn("%s", to.p_path);
		rval = 1;
		goto done;
	}

	rval = 0;

	if (!lflag && !sflag) {
		if (buf == NULL) {
			/*
			 * Note that buf and bufsize are static. If
			 * malloc() fails, it will fail at the start
			 * and not copy only some files. 
			 */ 
			if (sysconf(_SC_PHYS_PAGES) > 
			    PHYSPAGES_THRESHOLD)
				bufsize = MIN(BUFSIZE_MAX, 1 << 20);
			else
				bufsize = BUFSIZE_SMALL;
			buf = malloc(bufsize);
			if (buf == NULL)
				err(1, "Not enough memory");
		}
		wtotal = 0;
		do {
			if (use_copy_file_range) {
				rcount = copy_file_range(from_fd, NULL,
				    to_fd, NULL, SSIZE_MAX, 0);
				if (rcount < 0 && errno == EINVAL) {
					/* Prob a non-seekable FD */
					use_copy_file_range = 0;
				}
			}
			if (!use_copy_file_range) {
				rcount = copy_fallback(from_fd, to_fd,
				    buf, bufsize);
			}
			wtotal += rcount;
		} while (rcount > 0);
		if (rcount < 0) {
			warn("%s", path);
			rval = 1;
		}
	} else if (lflag) {
		if (link(path, to.p_path)) {
			warn("%s", to.p_path);
			rval = 1;
		}
	} else if (sflag) {
		if (symlink(path, to.p_path)) {
			warn("%s", to.p_path);
			rval = 1;
		}
	}

	/*
	 * Don't remove the target even after an error.  The target might
	 * not be a regular file, or its attributes might be important,
	 * or its contents might be irreplaceable.  It would only be safe
	 * to remove it if we created it and its length is 0.
	 */

	if (!lflag && !sflag) {
		if (pflag && setfile(fs, to_fd))
			rval = 1;
		if (close(to_fd)) {
			warn("%s", to.p_path);
			rval = 1;
		}
	}

done:
	if (from_fd != -1)
		(void)close(from_fd);
	return (rval);
}

static int
copy_link(const char *path, const struct stat *st, int exists)
{
	int len;
	char llink[PATH_MAX];

	if (exists && nflag) {
		if (vflag)
			printf("%s not overwritten\n", to.p_path);
		return (1);
	}
	if ((len = readlink(path, llink, sizeof(llink) - 1)) == -1) {
		warn("readlink: %s", path);
		return (1);
	}
	llink[len] = '\0';
	if (exists && unlink(to.p_path)) {
		warn("unlink: %s", to.p_path);
		return (1);
	}
	if (symlink(llink, to.p_path)) {
		warn("symlink: %s", llink);
		return (1);
	}
	return (pflag ? setfile(st, -1) : 0);
}

static int
copy_fifo(const struct stat *from_stat, int exists)
{

	if (exists && nflag) {
		if (vflag)
			printf("%s not overwritten\n", to.p_path);
		return (1);
	}
	if (exists && unlink(to.p_path)) {
		warn("unlink: %s", to.p_path);
		return (1);
	}
	if (mkfifo(to.p_path, from_stat->st_mode)) {
		warn("mkfifo: %s", to.p_path);
		return (1);
	}
	return (pflag ? setfile(from_stat, -1) : 0);
}

static int
copy_special(const struct stat *from_stat, int exists)
{

	if (exists && nflag) {
		if (vflag)
			printf("%s not overwritten\n", to.p_path);
		return (1);
	}
	if (exists && unlink(to.p_path)) {
		warn("unlink: %s", to.p_path);
		return (1);
	}
	if (mknod(to.p_path, from_stat->st_mode, from_stat->st_rdev)) {
		warn("mknod: %s", to.p_path);
		return (1);
	}
	return (pflag ? setfile(from_stat, -1) : 0);
}

static int
setfile(const struct stat *fs, int fd)
{
	static struct timespec tspec[2];
	struct stat ts;
	int rval, gotstat, islink, fdval;

	rval = 0;
	fdval = fd != -1;
	islink = !fdval && S_ISLNK(fs->st_mode);
	mode_t mode = fs->st_mode & (S_ISUID | S_ISGID | S_ISVTX |
	    S_IRWXU | S_IRWXG | S_IRWXO);

	tspec[0] = fs->st_atim;
	tspec[1] = fs->st_mtim;
	if (fdval ? futimens(fd, tspec) : utimensat(AT_FDCWD, to.p_path, tspec,
	    islink ? AT_SYMLINK_NOFOLLOW : 0)) {
		warn("utimensat: %s", to.p_path);
		rval = 1;
	}
	if (fdval ? fstat(fd, &ts) :
	    (islink ? lstat(to.p_path, &ts) : stat(to.p_path, &ts)))
		gotstat = 0;
	else {
		gotstat = 1;
		ts.st_mode &= S_ISUID | S_ISGID | S_ISVTX |
		    S_IRWXU | S_IRWXG | S_IRWXO;
	}
	/*
	 * Changing the ownership probably won't succeed, unless we're root
	 * or POSIX_CHOWN_RESTRICTED is not set.  Set uid/gid before setting
	 * the mode; current BSD behavior is to remove all setuid bits on
	 * chown.  If chown fails, lose setuid/setgid bits.
	 */
	if (!gotstat || fs->st_uid != ts.st_uid || fs->st_gid != ts.st_gid)
		if (fdval ? fchown(fd, fs->st_uid, fs->st_gid) :
		    (islink ? lchown(to.p_path, fs->st_uid, fs->st_gid) :
		    chown(to.p_path, fs->st_uid, fs->st_gid))) {
			if (errno != EPERM) {
				warn("chown: %s", to.p_path);
				rval = 1;
			}
			mode &= ~(S_ISUID | S_ISGID);
		}

	if (!gotstat || mode != ts.st_mode)
		if (fdval ? fchmod(fd, mode) :
		    (islink ? fchmodat(AT_FDCWD, to.p_path, mode, AT_SYMLINK_NOFOLLOW) && errno != ENOTSUP :
		    chmod(to.p_path, mode))) {
			warn("chmod: %s", to.p_path);
			rval = 1;
		}

	return (rval);
}

static void
usage(void)
{

	(void)fprintf(stderr, "%s\n%s\n",
	    "usage: cp [-R [-H | -L | -P]] [-f | -i | -n] [-alpsvx] "
	    "source_file target_file",
	    "       cp [-R [-H | -L | -P]] [-f | -i | -n] [-alpsvx] "
	    "source_file ... "
	    "target_directory");
	exit(1);
}
